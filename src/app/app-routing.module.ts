import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import {IngresoDineroComponent} from './pages/ingreso-dinero/ingreso-dinero.component';
import {IngresoDineroFijoComponent} from './pages/ingreso-dinero-fijo/ingreso-dinero-fijo.component';
import {ConceptosClavePage} from './pages/conceptos-clave/conceptos-clave.page';

//const loggedIn = (next) => map((user:any) => !!user === user.uid);...canActivate(loggedIn

const routes: Routes = [

  { path: 'ingreso-dinero', component: IngresoDineroComponent},
  { path: 'ingreso-dinero-fijo', component: IngresoDineroFijoComponent},
  {
    path: 'welcome',
    loadChildren: () => import('./pages/welcome/welcome.module').then( m => m.WelcomePageModule)
  },
  {
    path: '',
    loadChildren: () => import('./pages/tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'grafica',
    loadChildren: () => import('./pages/grafica/grafica.module').then(m => m.GraficaPageModule)
  },
  {
    path: 'wallet',
    loadChildren: () => import('./pages/wallet/wallet.module').then( m => m.WalletPageModule)
  },
  {
    path: 'profile',
    loadChildren: () => import('./pages/profile/profile.module').then( m => m.ProfilePageModule)
  },
  {
    path: 'welcome',
    loadChildren: () => import('./pages/welcome/welcome.module').then( m => m.WelcomePageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./pages/register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'edit-profile',
    loadChildren: () => import('./pages/edit-profile/edit-profile.module').then( m => m.EditProfilePageModule)
  },
  {
    path: 'forgot',
    loadChildren: () => import('./pages/forgot/forgot.module').then( m => m.ForgotPageModule)
  },
  {
    path: 'configuracion-gastos',
    loadChildren: () => import('./pages/configracion-gastos/configuracion-gastos.module').then(m => m.ConfiguracionGastosPageModule)
  },
  {
    path: 'transfer',
    loadChildren: () => import('./pages/agregar-gasto/transfer.module').then(m => m.TransferPageModule)
  },
  {
    path: 'transferFijo',
    loadChildren: () => import('./pages/agregar-gasto-fijo/transferFijo.module').then(m => m.TransferPageModule)
  },
  {
    path: 'activity',
    loadChildren: () => import('./pages/activity/activity.module').then( m => m.ActivityPageModule)
  },
  {
    path: 'add-contact',
    loadChildren: () => import('./pages/add-contact/add-contact.module').then( m => m.AddContactPageModule)
  },
  {
    path: 'conceptos-clave',
    loadChildren: () => import('./pages/conceptos-clave/conceptos-clave.module').then( m => m.ConceptosClavePageModule)
  },
  {
    path: 'cometidos-errores',
    loadChildren: () => import('./pages/cometidos-errores/cometidos-errores.module').then( m => m.CometidosErroresPagePageModule)
  },
  {
    path: 'categoria-financiera',
    loadChildren: () => import('./pages/catagoria-financiera/categoria-financiera.module').then( m => m.CategoriaFinancieraPagePageModule)
  },
  {
    path: 'bienestar-financiero',
    loadChildren: () => import('./pages/bienestar-financiero/bienestar-financiero.module').then( m => m.BienestarFinancieroPageModule)
  },
  {
    path: 'buenos-habitos',
    loadChildren: () => import('./pages/buenos-habitos/buenos-habitos.module').then( m => m.BuenosHabitosPageModule)
  },
  {
    path: 'add-budget',
    loadChildren: () => import('./pages/add-budget/add-budget.module').then( m => m.AddBudgetPageModule)
  },
  {
    path: 'payment',
    loadChildren: () => import('./pages/payment/payment.module').then( m => m.PaymentPageModule)
  },
  {
    path: 'success',
    loadChildren: () => import('./pages/success/success.module').then( m => m.SuccessPageModule)
  },
  {
    path: 'settings',
    loadChildren: () => import('./pages/settings/settings.module').then( m => m.SettingsPageModule)
  },
  {
    path: 'notification',
    loadChildren: () => import('./pages/notification/notification.module').then( m => m.NotificationPageModule)
  },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
