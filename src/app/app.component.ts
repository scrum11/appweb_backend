import {Component} from '@angular/core';

import {AlertController, MenuController, Platform} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {AuthenticationService} from './services/authentication.service';
import {Route, Router} from '@angular/router';
import {FirebaseService} from './services/firebase.service';
import {Usuario} from './interfaces';
import {LoginPage} from './pages/login/login.page';
import {NotificationsService} from './services/notifications.service';
import {element} from 'protractor';
import {InteractionService} from './services/interaction.service';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss']
})
export class AppComponent {

    login = false;
    showAlert = false;
    isVisible = true;

    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private auth: AuthenticationService,
        private router: Router,
        private firestore: FirebaseService,
        public notificationsService: NotificationsService,
        public alertController: AlertController,
        private menu: MenuController,
        public interactionService: InteractionService
    ) {
        this.initializeApp();
        this.auth.stateUser().subscribe(res => {
            if (res) {
                console.log('esta logueado');
                this.login = true;
                this.getDatosUser(res.uid);
                router.navigateByUrl('/tabs')
            } else {
                console.log('no esta logueado');
                this.login = false;
                router.navigateByUrl('/login')
            }
        });
    }


   //
   //  var link = document.getElementById('icon');
   //  if (this.isVisible == false){
   //  link.addEventListener('click', function() {
   //      this.isVisible = true
   //  });
   // }


    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleLightContent();
            this.splashScreen.hide();
        });
    }

    ngOnInit() {
        this.notificationsService.alert$.subscribe((res) => {
            this.showAlert = true;
        });
    }

    async infoAutor() {
        const alert = await this.alertController.create({
            header: `Acerca de`,
            subHeader: 'Autor: Sebastián Minchala',
            message: `Nombre de la aplicación: PerPlata`,
            buttons: ['OK'],
        });

        await alert.present();
    }

    logout() {
        this.auth.logout();
        this.router.navigate(['/login']);
    }

    perfil() {
        this.router.navigate(['/tabs/profile']);
    }

    getDatosUser(uid: string) {
        const path = 'Usuarios';
        this.firestore.getDoc<Usuario>(path, uid).subscribe(res => {
            if (res) {
            }
        });
    }
}
