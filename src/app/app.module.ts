import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouteReuseStrategy} from '@angular/router';

import {IonicModule, IonicRouteStrategy} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {ChartsModule} from 'ng2-charts';
import {AngularFireModule} from '@angular/fire';
import {environment} from '../environments/environment';
import {AngularFirestoreModule} from '@angular/fire/firestore';
import {FormsModule} from '@angular/forms';
import {IngresoDineroComponent} from './pages/ingreso-dinero/ingreso-dinero.component';
import {IngresoDineroFijoComponent} from './pages/ingreso-dinero-fijo/ingreso-dinero-fijo.component';


@NgModule({
    declarations: [
        AppComponent,
        IngresoDineroComponent,
        IngresoDineroFijoComponent,
        ],
    entryComponents: [],
    imports: [
        BrowserModule,
        IonicModule.forRoot(),
        AppRoutingModule,
        ChartsModule,
        AngularFireModule.initializeApp(environment.firebaseConfig),
        AngularFirestoreModule,
        FormsModule,
    ],
    providers: [
        StatusBar,
        SplashScreen,
        IngresoDineroFijoComponent,
        IngresoDineroComponent,

        {provide: RouteReuseStrategy, useClass: IonicRouteStrategy}
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
