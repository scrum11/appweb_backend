import {Component, OnInit} from '@angular/core';
import {NavController} from '@ionic/angular';
import {Gasto, Ingreso} from '../models/models.component';
import {FirebaseService} from '../../services/firebase.service';
import {Usuario} from '../../interfaces';
import firebase from 'firebase';
import Timestamp = firebase.firestore.Timestamp;
import {AuthenticationService} from '../../services/authentication.service';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'app-activity',
    templateUrl: './activity.page.html',
    styleUrls: ['./activity.page.scss'],
})
export class ActivityPage implements OnInit {

    gastos: Gasto[] = [];
    gastosFijos: Gasto[] = [];
    ingresos: Ingreso[] = [];
    ingresosFijos: Ingreso[] = [];
    transacciones: any[] = [];
    sumaG: number = null;
    sumaI: number = null;
    sumaIfijos: number = null;
    sumaGFijos: number = null;
    sumaTotalIngresos: number = null;
    sumaTotalGastos:number = null;
    tabID = null;


    constructor(private navCtrl: NavController,
                public firebaseService: FirebaseService,
                public auth: AuthenticationService,
                public route: ActivatedRoute,
                ) {}

    ngOnInit() {
        this.route.queryParams
            .subscribe(params => {
                console.log(Array.isArray(params))
                console.log('params', params); // { order: "popular"
                this.auth.stateUser().subscribe(res => {
                    if (res) {
                        const pathIngresos = 'Usuarios/' + res.uid + '/Ingresos';
                        const pathIngresosFijos = 'Usuarios/' + res.uid + '/Ingresos Fijos';
                        const pathGastos = 'Usuarios/' + res.uid + '/Gastos';
                        const pathGastosFijos = 'Usuarios/' + res.uid + '/Gastos Fijos';

                        const fechaActual = new Date();
                        this.tabID = fechaActual.getMonth();
                        console.log(this.tabID);
                        console.log('this.tabId', this.tabID);

                        //Fecha inicio
                        const fechaInicio = new Date();
                        fechaInicio.setMonth(+params.mesElegido, 1);
                        fechaInicio.setHours(0, 0, 0);
                        console.log('Parametros pasados',+params.mesElegido);
                        //Fecha final
                        const fechaFin = new Date();
                        fechaFin.setMonth(+params.mesElegido, 30);
                        fechaFin.setHours(23, 59, 59);

                        this.firebaseService.getCollection2Query<Gasto>(pathGastos, 'fecha', '>=', fechaInicio,
                            'fecha', '<=', fechaFin).subscribe(async res => {
                            this.gastos = res;
                            this.sumaG = 0;
                            this.gastos.forEach(gasto => {
                                this.sumaG += gasto.monto_gasto;
                                gasto.fecha = new Date(gasto.fecha.seconds * 1000);
                            });
                            this.sumaGastos();
                        });

                        this.firebaseService.getCollection2Query<Gasto>(pathGastosFijos, 'fecha', '>=', fechaInicio,
                            'fecha', '<=', fechaFin).subscribe(res => {
                            this.gastosFijos = res;
                            this.sumaGFijos = 0;
                            this.gastosFijos.forEach(gFijo => {
                                this.sumaGFijos += gFijo.monto_gasto;
                                gFijo.fecha = new Date(gFijo.fecha.seconds * 1000);
                            });
                            this.sumaGastos();
                        });

                        this.firebaseService.getCollection2Query<Ingreso>(pathIngresos, 'fecha', '>=', fechaInicio,
                            'fecha', '<=', fechaFin).subscribe(res => {
                            this.ingresos = res;
                            this.sumaI = 0;
                            this.ingresos.forEach(ingreso => {
                                this.sumaI += ingreso.monto_ingreso;
                                ingreso.fecha = new Date(ingreso.fecha.seconds * 1000);
                            });
                            this.sumaIngresos();
                        });

                        this.firebaseService.getCollection2Query<Ingreso>(pathIngresosFijos, 'fecha', '>=', fechaInicio,
                            'fecha', '<=', fechaFin).subscribe(res => {
                            this.ingresosFijos = res;
                            this.sumaIfijos = 0;
                            this.ingresosFijos.forEach(ingreso => {
                                this.sumaIfijos += ingreso.monto_ingreso;
                                ingreso.fecha = new Date(ingreso.fecha.seconds * 1000);
                            });
                            this.sumaIngresos();
                        });
                    }
                });
            })
    }

    sumaGastos() {
        this.sumaTotalGastos = this.sumaG + this.sumaGFijos;
        console.log('Suma total de gastos', this.sumaTotalGastos);
        this.setTransacciones()
    }

    sumaIngresos() {
        this.sumaTotalIngresos = this.sumaI + this.sumaIfijos;
        console.log('Suma total de ingresos', this.sumaTotalIngresos);
        this.setTransacciones()
    }

    // async getGastos() {
    //     const uid = await this.auth.getUid();
    //     const ruta = 'Usuarios/' + uid + '/Gastos';
    //     this.firebaseService.getCollection<Gasto>(ruta).subscribe(res => {
    //         this.gastos = res;
    //         this.sumaG = 0;
    //         this.gastos.forEach(gasto => {
    //             this.sumaG += gasto.monto_gasto;
    //             gasto.fecha = new Date(gasto.fecha.seconds * 1000);
    //         });
    //         this.setTransacciones();
    //     });
    // }

    // async getIngresos() {
    //     const uid = await this.auth.getUid();
    //     const ruta = 'Usuarios/' + uid + '/Ingresos';
    //     console.log('La ruta de activity es:', ruta);
    //     this.firebaseService.getCollection<Ingreso>(ruta).subscribe(res => {
    //         this.ingresos = res;
    //         this.sumaI = 0;
    //         this.ingresos.forEach(ingreso => {
    //             this.sumaI += ingreso.monto_ingreso;
    //             ingreso.fecha = new Date(ingreso.fecha.seconds * 1000);
    //         });
    //         this.setTransacciones();
    //     });
    // }


    setTransacciones() {
        this.transacciones = [];
        this.gastos.forEach(gasto => {
            this.transacciones.push(gasto);
        });
        this.gastosFijos.forEach(gfijo => {
            this.transacciones.push(gfijo)
        })
        this.ingresos.forEach(ingreso => {
                this.transacciones.push(ingreso);
            });
        this.ingresosFijos.forEach(ifijos => {
            this.transacciones.push(ifijos)
        })
        this.transacciones.sort((a, b) => {
            if (a.fecha > b.fecha) {
                return -1;
            }
            if (a.fecha < b.fecha) {
                return 1;
            }
            if (a.fecha == b.fecha) {
                return 0;
            }
        });
    }

    goBack() {
        this.navCtrl.back();
    }

}
