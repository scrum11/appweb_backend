import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TransferFijoPage } from './transferFijo.page';

const routes: Routes = [
  {
    path: '',
    component: TransferFijoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TransferPageRoutingModule {}
