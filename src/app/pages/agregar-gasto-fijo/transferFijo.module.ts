import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TransferPageRoutingModule } from './transferFijo-routing.module';

import { TransferFijoPage } from './transferFijo.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TransferPageRoutingModule
  ],
  declarations: [TransferFijoPage]
})
export class TransferPageModule{}
