import {Component, OnInit} from '@angular/core';
import {AlertController, LoadingController, NavController, ToastController} from '@ionic/angular';
import {Gasto, Notificacion} from '../models/models.component';
import {FirebaseService} from '../../services/firebase.service';
import {AuthenticationService} from '../../services/authentication.service';
import {InteractionService} from '../../services/interaction.service';
import {NotificationsService} from '../../services/notifications.service';

@Component({
    selector: 'app-agregar-gasto',
    templateUrl: './transferFijo.page.html',
    styleUrls: ['./transferFijo.page.scss'],
})
export class TransferFijoPage implements OnInit {

    // typeFrecuencia: 'diario' | 'semanal' | 'mensual' = null;

    constructor(private navCtrl: NavController,
                public firebaseService: FirebaseService,
                public loadingController: LoadingController,
                public toastController: ToastController,
                public alertController: AlertController,
                private auth: AuthenticationService,
                public interactionService:InteractionService,
                public notificationsService: NotificationsService) {
    }


    gastos: Gasto[] = [];
    loading: any;

    newGastoFijo: Gasto = {
        categoria: null,
        monto_gasto: null,
        descripcion_gasto: null,
        tipo_transaccion: 'gasto-fijo',
        fecha: new Date(),
        id: this.firebaseService.getId()
    };


    ngOnInit() {
        this.auth.stateUser().subscribe(res => {
            if (res) {
                this.getGasto();
            }
        });
    }

    // selecfrecuencia(type: 'diario' | 'semanal' | 'mensual') {
    //     this.typeFrecuencia = type;
    //     return;
    // }

    goBack() {
        this.navCtrl.back();
    }

    async guardarGastos() {
        const uid = await this.auth.getUid();
        const ruta = 'Usuarios/' + uid + '/Gastos Fijos/';
        // this.newGasto.typeFrecuencia = this.typeFrecuencia;
        this.firebaseService.createDoc(this.newGastoFijo, ruta, this.newGastoFijo.id).then(res => {
            this.presentToast('Guardado con exito');
            this.loading.dismiss();
            this.notificationsService.notificacionInfo = true;
            this.nuevoEg()
            // const suceso = new Date().toLocaleString();
            // this.notificationsService.guardarNotification('Se ha guardado el gasto fijo' +
            //     this.newGastoFijo.descripcion_gasto, 'informacion', true, suceso)
        })
        //     .catch(error => {
        //     this.presentToast('No se pudo guardar');
        // });
    }

    async getGasto() {
        const uid = await this.auth.getUid();
        const ruta = 'Usuarios/' + uid + '/Gastos Fijos';
        this.firebaseService.getCollection<Gasto>(ruta).subscribe(res => {
            this.gastos = res;
        });
    }

    async deleteGasto(gasto: Gasto) {
        const uid = await this.auth.getUid();
        const ruta = 'Usuarios/' + uid + '/Gastos Fijos';
        const alert = await this.alertController.create({
            cssClass: 'normal',
            header: 'Eliminar gasto fijo',
            message: 'Esta acción <strong>eliminará</strong> este gasto fijo',
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (blah) => {
                    }
                }, {
                    text: 'Confirmar',
                    handler: () => {
                        this.firebaseService.deleteDoc(ruta, gasto.id).then(res => {
                            this.presentToast('Eliminado con exito');
                            this.alertController.dismiss();
                        }).catch(error => {
                            //this.interactionService.presentToast('No se pudo eliminar');
                        });
                    }
                }
            ]
        });
        await alert.present();
    }

    async presentLoading() {
        this.loading = await this.loadingController.create({
            cssClass: 'normal',
            message: 'Guardando...'
        });
        await this.loading.present();
        //
        // const { role, data } = await loading.onDidDismiss();
        // console.log('Loading dismissed!');
    }

    async presentToast(msg: string) {
        const toast = await this.toastController.create({
            message: msg,
            cssClass: 'normal',
            duration: 1000,
        });
        toast.present();
    }

    nuevoEg() {
        // this.enableNewGasto = true;
        this.newGastoFijo = {
            // typeFrecuencia: null,
            categoria: null,
            monto_gasto: null,
            descripcion_gasto: null,
            tipo_transaccion: 'gasto-fijo',
            fecha: new Date(),
            id: this.firebaseService.getId()
        };
    }
}
