import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TransferPageRoutingModule } from './transfer-routing.module';

import { TransferPage } from './transfer.page';
import {HomePageModule} from '../home/home.module';
import {HomePage} from '../home/home.page';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        TransferPageRoutingModule,
        HomePageModule
    ],
    declarations: [TransferPage],
    exports: [

    ],
    providers: [
        HomePage
    ]

})
export class TransferPageModule{}
