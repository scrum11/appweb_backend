import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {AlertController, LoadingController, NavController, ToastController} from '@ionic/angular';
import {Categorias, Gasto, Ingreso} from '../models/models.component';
import {FirebaseService} from '../../services/firebase.service';
import {AuthenticationService} from '../../services/authentication.service';
import {HomePage} from '../home/home.page';
import {InteractionService} from '../../services/interaction.service';
import {ActivatedRoute, Router, RouterState} from '@angular/router';

@Component({
    selector: 'app-agregar-gasto',
    templateUrl: './transfer.page.html',
    styleUrls: ['./transfer.page.scss'],
})
export class TransferPage implements OnInit {

    // typeFrecuencia: 'inmediata' | 'semanal' | 'mensual' = null;

    newGasto: Gasto = {
        categoria: null,
        monto_gasto: null,
        descripcion_gasto: null,
        tipo_transaccion: 'gasto',
        fecha: new Date(),
        id: this.firebaseService.getId()
    };

    gastos: Gasto[] = [];
    loading: any;
    orden: any[] = [];
    tabID = null;
    categorias: Categorias[] = [];


    constructor(private navCtrl: NavController,
                public firebaseService: FirebaseService,
                public loadingController: LoadingController,
                public toastController: ToastController,
                public alertController: AlertController,
                private auth: AuthenticationService,
                public interactionService: InteractionService,
                public route: ActivatedRoute
    ) {

    }

    ngOnInit() {
        this.route.queryParams
            .subscribe(params => {
                    this.auth.stateUser().subscribe(res => {
                        if (res) {
                            const path = 'Usuarios/' + res.uid + '/Gastos';
                            const fechaActual = new Date();
                            this.tabID = fechaActual.getMonth();
                            console.log(this.tabID);
                            //Fecha inicio
                            const fechaInicio = new Date();
                            fechaInicio.setMonth(+params.mesElegido, 1);
                            fechaInicio.setHours(0, 0, 0);
                            console.log('Parametros pasados', +params.mesElegido);

                            //Fecha final
                            const fechaFin = new Date();
                            fechaFin.setMonth(+params.mesElegido, 30);
                            fechaFin.setHours(23, 59, 59);


                            this.firebaseService.getCollection2Query<Gasto>(path, 'fecha', '>=', fechaInicio,
                                'fecha', '<=', fechaFin).subscribe(async res => {
                                this.gastos = res;
                                this.gastos.forEach(gasto => {
                                    gasto.fecha = new Date(gasto.fecha.seconds * 1000);
                                });
                                this.ordenar();
                                this.getCategorias();
                            });
                        }
                    });
                }
            );
    }

    goBack() {
        this.navCtrl.back();
    }

    ordenar() {
        this.orden = [];
        this.gastos.forEach(gasto => {
            this.orden.push(gasto);
        });
        this.orden.sort((a, b) => {
            if (a.fecha > b.fecha) {
                return -1;
            }
            if (a.fecha < b.fecha) {
                return 1;
            }
            if (a.fecha == b.fecha) {
                return 0;
            }
        });
    }

    async getCategorias() {
        const uid = await this.auth.getUid();
        const ruta = 'Usuarios/' + uid + '/Categorias Activas';
        this.categorias = [];
        this.firebaseService.getCollection<Categorias>(ruta).subscribe(res => {
            this.categorias = res;
        });
        console.log("Categorias activas array", this.categorias);
    }

    async guardarGastos() {
        const uid = await this.auth.getUid();
        const ruta = 'Usuarios/' + uid + '/Gastos/';
        this.firebaseService.createDoc(this.newGasto, ruta, this.newGasto.id).then(res => {
            this.presentToast('Guardado con exito');
        });
        this.nuevoEg();
    }

    // async getGasto() {
    //     const uid = await this.auth.getUid();
    //     const ruta = 'Usuarios/' + uid + '/Gastos';
    //     this.firebaseService.getCollection<Gasto>(ruta).subscribe(res => {
    //         this.gastos = res;
    //         console.log('los gastos son ', this.gastos, ' ', ruta);
    //     });
    // }

    async deleteGasto(gasto: Gasto) {
        const uid = await this.auth.getUid();
        const ruta = 'Usuarios/' + uid + '/Gastos';
        const alert = await this.alertController.create({
            cssClass: 'normal',
            header: 'Eliminar gasto',
            message: 'Esta acción <strong>eliminará</strong> este gasto',
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (blah) => {
                    }
                }, {
                    text: 'Confirmar',
                    handler: () => {
                        this.firebaseService.deleteDoc(ruta, gasto.id).then(res => {
                            this.presentToast('Eliminado con exito');
                            this.alertController.dismiss();
                        })
                    }
                }
            ]
        });

        await alert.present();
    }

    nuevoEg() {
        // this.enableNewGasto = true;
        this.newGasto = {
            typeFrecuencia: null,
            categoria: null,
            monto_gasto: null,
            descripcion_gasto: null,
            tipo_transaccion: 'gasto',
            fecha: new Date(),
            id: this.firebaseService.getId()
        };
    }

    async presentLoading() {
        this.loading = await this.loadingController.create({
            cssClass: 'normal',
            message: 'Guardando...'
        });
        await this.loading.present();
        //
        // const { role, data } = await loading.onDidDismiss();
        // console.log('Loading dismissed!');
    }

    async presentToast(msg: string) {
        const toast = await this.toastController.create({
            message: msg,
            cssClass: 'normal',
            duration: 2000,
        });
        toast.present();
    }

}
