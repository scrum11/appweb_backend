import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BienestarFinancieroPage } from './bienestar-financiero.page';

const routes: Routes = [
  {
    path: '',
    component: BienestarFinancieroPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BienestarFinancieroPageRoutingModule {}
