import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BienestarFinancieroPage } from './bienestar-financiero.page';
import {BienestarFinancieroPageRoutingModule} from './bienestar-financiero-routing.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BienestarFinancieroPageRoutingModule
  ],
  exports: [
    BienestarFinancieroPage
  ],
  declarations: [BienestarFinancieroPage]
})
export class BienestarFinancieroPageModule {}

