import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BuenosHabitosPage } from './buenos-habitos.page';

const routes: Routes = [
  {
    path: '',
    component: BuenosHabitosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BuenosHabitosPageRoutingModule {}
