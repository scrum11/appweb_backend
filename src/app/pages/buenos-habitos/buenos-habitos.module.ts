import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BuenosHabitosPage } from './buenos-habitos.page';
import {BuenosHabitosPageRoutingModule} from './buenos-habitos-routing.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BuenosHabitosPageRoutingModule
  ],
  exports: [
    BuenosHabitosPage
  ],
  declarations: [BuenosHabitosPage]
})
export class BuenosHabitosPageModule {}

