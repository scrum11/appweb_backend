import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CategoriaFinancieraPage } from './categoria-financiera.page';

const routes: Routes = [
  {
    path: '',
    component: CategoriaFinancieraPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CategoriaFinancieraPageRoutingModule {}
