import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CategoriaFinancieraPage } from './categoria-financiera.page';
import {CategoriaFinancieraPageRoutingModule} from './categoria-financiera-routing.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CategoriaFinancieraPageRoutingModule
  ],
  exports: [
    CategoriaFinancieraPage
  ],
  declarations: [CategoriaFinancieraPage]
})
export class CategoriaFinancieraPagePageModule {}

