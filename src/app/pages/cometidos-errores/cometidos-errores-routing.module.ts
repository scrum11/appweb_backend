import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CometidosErroresPage } from './cometidos-errores.page';

const routes: Routes = [
  {
    path: '',
    component: CometidosErroresPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CometidosErroresPageRoutingModule {}
