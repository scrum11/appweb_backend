import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConceptosClavePage } from './conceptos-clave.page';

const routes: Routes = [
  {
    path: '',
    component: ConceptosClavePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConceptosClavePageRoutingModule {}
