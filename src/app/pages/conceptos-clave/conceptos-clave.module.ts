import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConceptosClavePage } from './conceptos-clave.page';
import {ConceptosClavePageRoutingModule} from './conceptos-clave-routing.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConceptosClavePageRoutingModule
  ],
  exports: [
    ConceptosClavePage
  ],
  declarations: [ConceptosClavePage]
})
export class ConceptosClavePageModule {}

