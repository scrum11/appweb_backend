import {AlertController, ModalController, NavController} from '@ionic/angular';
import {Router} from '@angular/router';
import {Component} from '@angular/core';
import {GraficaPage} from '../grafica/grafica.page';
import {DomSanitizer} from '@angular/platform-browser';


@Component({
    selector: 'add-budget',
    templateUrl: './conceptos-clave.page.html',
    styleUrls: ['./conceptos-clave.page.scss']
})
export class ConceptosClavePage {
    // Typically referenced to your ion-router-outlet

    date; //date Variable
    logedInForm; //These are variables
    emailId;
    password;
    display='none'; //default Variable

    constructor(private navCtrl: NavController,
                public router: Router,
                public modalController: ModalController,
                public alertController: AlertController,
                public sanitizer:DomSanitizer,
    ) {

    }

    ngOnInit() {
    }


    goBack() {
        this.navCtrl.back();
    }


    async presentAlert1() {
        const alert = await this.alertController.create({
            header: 'Crea un presupuesto',
            message: `Podría decirse que este es el consejo de dinero más esencial que existe: haga un presupuesto (y apéguese a él).

El autor John C. Maxwell explica la elaboración de presupuestos a la perfección: “Un presupuesto le dice a su dinero adónde debe ir en lugar de preguntarse adónde fue a parar”.
`,
            buttons: ['OK'],
        });
        await alert.present();
    }

    async presentAlert2() {
        const alert = await this.alertController.create({
            header: 'Manténgase organizado con aplicaciones de presupuesto',
            message: `Aprender a presupuestar su dinero no es una tarea fácil, así que ¿por qué no hacerlo más fácil para usted?

Otro consejo importante sobre finanzas personales es usar aplicaciones de presupuesto .
`,
            buttons: ['OK'],
        });
        await alert.present();
    }

    async presentAlert3() {
        const alert = await this.alertController.create({
            header: 'No haga compras impulsivas',
            message: `Todo el mundo hace compras impulsivas de vez en cuando, pero pueden agotar rápidamente su cuenta bancaria.

Entonces, la próxima vez que vea algo que simplemente 'tiene' que comprar, espere una semana antes de entregar su efectivo.

El tiempo te dará espacio para cierta perspectiva. Entonces, si aún desea comprarlo, sabrá que definitivamente vale su dinero. Lo más probable es que decidas quedarte con tu dinero.

Como dijo el caricaturista y periodista Kin Hubbard: “La forma más segura de duplicar su dinero es doblarlo y guardarlo en su bolsillo”.
`,
            buttons: ['OK'],
        });
        await alert.present();
    }

    async presentAlert4() {
        const alert = await this.alertController.create({
            header: 'Anota todas tus deudas',
            message: `Saldar deudas puede ser difícil, pero primero debe crear una lista de todo lo que debe. En esta lista, debe mencionar la cantidad que debe, la cantidad de pagos mínimos, la duración del préstamo y su tasa de interés actual.

Una vez que complete esta lista, puede crear un plan sobre lo que debe pagar primero. Trate de terminarlos en los próximos 12 meses y esfuércese por lograrlo, ya que eso lo ayudaría a mejorar su posición financiera.
`,

            buttons: ['OK'],
        });
        await alert.present();
    }

    async presentAlert5() {
        const alert = await this.alertController.create({
            header: 'Necesita ganar más dinero',
            message: `Todos escuchamos sobre presupuestos, recortes y ser conscientes de sus gastos. Pero en algún momento, golpeas una pared. Necesitas aumentar tu salario y ganar más dinero.

En su trabajo, encuentre formas de aumentar sus habilidades, asumir más, aprender en su tiempo libre. Pide un aumento de sueldo , conoce el valor de tu carrera y apresúrate a conseguir el salario que te mereces. No será fácil, pero es necesario.
`,
            buttons: ['OK'],
        });
        await alert.present();
    }

}
