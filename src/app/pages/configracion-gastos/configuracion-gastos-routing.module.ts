import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConfiguracionGastosPage } from './configuracion-gastos.page';

const routes: Routes = [
  {
    path: '',
    component: ConfiguracionGastosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConfiguracionGastosRoutingModule {}
