import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConfiguracionGastosPage } from './configuracion-gastos.page';
import { ConfiguracionGastosRoutingModule } from './configuracion-gastos-routing.module';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ConfiguracionGastosRoutingModule
    ],
    exports: [
        ConfiguracionGastosPage
    ],
    declarations: [ConfiguracionGastosPage]
})
export class ConfiguracionGastosPageModule {}
