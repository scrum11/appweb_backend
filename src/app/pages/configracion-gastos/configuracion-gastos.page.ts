import {Component, OnInit} from '@angular/core';
import {AlertController, LoadingController, NavController, ToastController} from '@ionic/angular';
import {InteractionService} from '../../services/interaction.service';
import {FirebaseService} from '../../services/firebase.service';
import {AuthenticationService} from '../../services/authentication.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Categorias} from '../models/models.component';

@Component({
    selector: 'app-configracion-gastos',
    templateUrl: './configuracion-gastos.page.html',
    styleUrls: ['./configuracion-gastos.page.scss'],
})
export class ConfiguracionGastosPage implements OnInit {

    newCategoria: Categorias = {
        hogar: true,
        comida: true,
        educacion: true,
        compras: true,
        transporte: true,
        entretenimiento: true,
        mascotas: true,
        suscripcion: true,
        deudas: true,
        otros: true,
        id: 'categorias'
    };

    constructor(private navCtrl: NavController,
                public interactionService: InteractionService,
                public firebaseService: FirebaseService,
                public loadingController: LoadingController,
                public toastController: ToastController,
                public alertController: AlertController,
                private auth: AuthenticationService,
                public route: ActivatedRoute,
                public router: Router
    ) {
    }

    ngOnInit() {
        this.getCategorias();
    }

    goBack() {
        this.router.navigate['/tabs/profile'];
    }

    async getCategorias() {
        const uid = await this.auth.getUid();
        const ruta = 'Usuarios/' + uid + '/Categorias Activas';
        console.log('ruta', ruta);
        this.firebaseService.getDoc<Categorias>(ruta, this.newCategoria.id).subscribe(res => {
            if (res){
            this.newCategoria = res
                console.log('newCategoria', this.newCategoria);
            }
        });
    }

    async guardarCategoria() {
        const uid = await this.auth.getUid();
        const ruta = 'Usuarios/' + uid + '/Categorias Activas';
        this.interactionService.presentToast('Guardando...');
        this.firebaseService.createDoc(this.newCategoria, ruta, this.newCategoria.id).then(res => {
            this.interactionService.closeLoading();
            this.interactionService.presentToast('Guardado con exito');
            this.router.navigate(['/tabs/home'])
        }).catch(error => {
            this.interactionService.presentToast('No se pudo guardar');
        });
    }

}
