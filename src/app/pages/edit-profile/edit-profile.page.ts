import {Component, OnInit} from '@angular/core';
import {AlertController, NavController} from '@ionic/angular';
import {Usuario} from '../models/models.component';
import {Router} from '@angular/router';
import {AuthenticationService} from '../../services/authentication.service';
import {FirebaseService} from '../../services/firebase.service';
import {ProfilePage} from '../profile/profile.page';
import {InteractionService} from '../../services/interaction.service';

@Component({
    selector: 'app-edit-profile',
    templateUrl: './edit-profile.page.html',
    styleUrls: ['./edit-profile.page.scss'],
})
export class EditProfilePage implements OnInit {

    uid: string = null;
    info: Usuario = null;

    constructor(private navCtrl: NavController,
                private router: Router,
                private auth: AuthenticationService,
                private firestoreService: FirebaseService,
                public alertController: AlertController,
                public interactionService: InteractionService
    ) {
    }

    ngOnInit() {
        this.auth.stateUser().subscribe(res => {
            this.getUid();
        });
    }

    async getUid() {
        const uid = await this.auth.getUid();
        if (uid) {
            this.uid = uid;
            this.getInfoUser()
        } else {
            console.log('No existe uid');
        }
        console.log('El id de usuario es:', this.uid);
    }

    getInfoUser() {
        const path = 'Usuarios/';
        const id = this.uid;
        this.firestoreService.getDoc<Usuario>(path, id).subscribe(res => {
            if (res) {
                this.info = res;
            }
            console.log('datos son:', res.usuario);
        });
    }

    async guardarCambios() {
        const path = 'Usuarios/';
        const id = this.uid;
        const updateDoc = {
            edad: this.info.edad,
            usuario: this.info.usuario,
            correo: this.info.correo
        };
        // updateDoc[edad] = input;
        await this.firestoreService.updateDoc(path, id, updateDoc).then(() => {
            this.interactionService.presentToast("Guardado con exito")
            this.interactionService.closeLoading()
            this.router.navigate(['/tabs/profile'])
        });
    }

    goBack() {
        this.navCtrl.back();
    }

}
