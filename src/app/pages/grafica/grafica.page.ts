import {Component, OnInit, ViewChild} from '@angular/core';
import {ChartDataSets, ChartOptions, ChartType} from 'chart.js';
import {Color, BaseChartDirective, Label} from 'ng2-charts';
import firebase from 'firebase';
import {AuthenticationService} from '../../services/authentication.service';
import {Gasto, Ingreso} from '../models/models.component';
import {FirebaseService} from '../../services/firebase.service';
import {HomePage} from '../home/home.page';
import {NotificationsService} from '../../services/notifications.service';

// import * as pluginAnnotations from 'chartjs-plugin-annotation';

@Component({
    selector: 'app-grafica',
    templateUrl: './grafica.page.html',
    styleUrls: ['./grafica.page.scss'],
})
export class GraficaPage implements OnInit {

    inv: any[] = [];
    gastos: Gasto[] = [];
    gastosFijos: Gasto[] = [];
    ingresos: Ingreso[] = [];
    ingresosFijos: Ingreso[] = [];
    sumaG: number = null;
    sumaI: number = null;
    sumaIfijos: number = null;
    sumaGFijos: number = null;
    sumaTotalIngresos: number = null;
    sumaTotalGastos: number = null;
    ingresos2: any[] = [];
    ingresos3: any[] = [];
    ingresosArray: any[] = [];
    gastos2: any[] = [];
    gastos3: any[] = [];
    gastosArray: any[] = [];
    tabID = null;

    //De traer info por fechas
    mesActual = null;

    constructor(public auth: AuthenticationService,
                public firebaseService: FirebaseService,
                public notificationsService: NotificationsService
    ) {
    }

    ngOnInit() {
        this.auth.stateUser().subscribe(res => {
            if (res) {
                const pathIngresos = 'Usuarios/' + res.uid + '/Ingresos';
                const pathIngresosFijos = 'Usuarios/' + res.uid + '/Ingresos Fijos';
                const pathGastos = 'Usuarios/' + res.uid + '/Gastos';
                const pathGastosFijos = 'Usuarios/' + res.uid + '/Gastos Fijos';

                const fechaActual = new Date();
                this.tabID = fechaActual.getMonth();
                this.operarMes(this.tabID);

                //if (this.tabID) {
                //Fecha inicial
                const fechaInicio = new Date();
                fechaInicio.setDate(1);
                fechaInicio.setHours(0);
                const formatter = new Intl.DateTimeFormat('ec', {month: 'long'});
                const month1 = formatter.format(fechaInicio);
                this.mesActual = month1;
                //Fecha final
                const fechaFin = new Date();
                fechaFin.setDate(30);
                fechaFin.setHours(23, 59);

                //Operando el mes actual
                this.firebaseService.getCollection2Query<Ingreso>(pathIngresos, 'fecha', '>=', fechaInicio,
                    'fecha', '<=', fechaFin).subscribe(res => {
                    this.ingresos = res;
                    this.sumaI = 0;
                    this.ingresos2 = [];
                    this.ingresos.forEach(ingreso => {
                        this.sumaI += ingreso.monto_ingreso;
                        this.ingresos2.push(ingreso.monto_ingreso);
                    });
                    this.sumaIngresos();
                });
                this.firebaseService.getCollection2Query<Ingreso>(pathIngresosFijos, 'fecha', '>=', fechaInicio,
                    'fecha', '<=', fechaFin).subscribe(res => {
                    this.ingresosFijos = res;
                    this.sumaIfijos = 0;
                    this.ingresos2 = [];
                    this.ingresosFijos.forEach(ifijo => {
                        this.sumaIfijos += ifijo.monto_ingreso;
                        this.ingresos2.push(ifijo.monto_ingreso);
                    });
                    this.sumaIngresos();
                });

                this.firebaseService.getCollection2Query<Gasto>(pathGastos, 'fecha', '>=', fechaInicio,
                    'fecha', '<=', fechaFin).subscribe(res => {
                    this.gastos = res;
                    this.sumaG = 0;
                    this.gastos2 = [];
                    this.gastos.forEach(gasto => {
                        this.sumaG += gasto.monto_gasto;
                    this.gastos2.push(gasto.monto_gasto);
                    });
                    this.sumaGastos();
                });
                //}
                this.firebaseService.getCollection2Query<Gasto>(pathGastosFijos, 'fecha', '>=', fechaInicio,
                    'fecha', '<=', fechaFin).subscribe(res => {
                    this.gastosFijos = res;
                    this.sumaGFijos = 0;
                    this.gastos2 = [];
                    this.gastosFijos.forEach(gfijo => {
                        this.sumaGFijos += gfijo.monto_gasto;
                        this.gastos2.push(gfijo.monto_gasto);
                    });
                    this.sumaGastos();
                });

            }
        });
    }


    operarMes(mes: number) {
        this.auth.stateUser().subscribe(res => {
            if (res) {
                const pathIngresos = 'Usuarios/' + res.uid + '/Ingresos';
                const pathIngresosFijos = 'Usuarios/' + res.uid + '/Ingresos Fijos';
                const pathGastos = 'Usuarios/' + res.uid + '/Gastos';
                const pathGastosFijos = 'Usuarios/' + res.uid + '/Gastos Fijos';

                const fechaActual = new Date();
                this.tabID = fechaActual.getMonth();

                //if (this.tabID) {
                //Fecha inicial
                const fechaInicio = new Date();
                fechaInicio.setMonth(mes, 1);
                fechaInicio.setHours(0, 0, 0);
                const formatter = new Intl.DateTimeFormat('ec', {month: 'long'});
                const month1 = formatter.format(fechaInicio);
                this.mesActual = month1;
                console.log("Mes actual", this.mesActual);
                //Fecha final
                const fechaFin = new Date();
                fechaFin.setMonth(mes, 30);
                fechaFin.setHours(23, 59, 59);

                //Operando el mes actual
                this.firebaseService.getCollection2Query<Ingreso>(pathIngresos, 'fecha', '>=', fechaInicio,
                    'fecha', '<=', fechaFin).subscribe(res => {
                    this.ingresos = res;
                    this.sumaI = 0;
                    this.ingresos2 = [];
                    this.ingresos.forEach(ingreso => {
                        this.sumaI += ingreso.monto_ingreso;
                        this.ingresos2.push(ingreso.monto_ingreso);
                    });
                    this.sumaIngresos();
                });
                this.firebaseService.getCollection2Query<Ingreso>(pathIngresosFijos, 'fecha', '>=', fechaInicio,
                    'fecha', '<=', fechaFin).subscribe(res => {
                    this.ingresosFijos = res;
                    this.sumaIfijos = 0;
                    this.ingresos3 = [];
                    this.ingresosFijos.forEach(ifijo => {
                        this.sumaIfijos += ifijo.monto_ingreso;
                        this.ingresos3.push(ifijo.monto_ingreso);
                    });
                    this.sumaIngresos();
                });

                this.firebaseService.getCollection2Query<Gasto>(pathGastos, 'fecha', '>=', fechaInicio,
                    'fecha', '<=', fechaFin).subscribe(res => {
                    this.gastos = res;
                    this.sumaG = 0;
                    this.gastos2 = [];
                    this.gastos.forEach(gasto => {
                        this.sumaG += gasto.monto_gasto;
                        this.gastos2.push(gasto.monto_gasto);
                    });
                    this.sumaGastos();
                });
                //}
                this.firebaseService.getCollection2Query<Gasto>(pathGastosFijos, 'fecha', '>=', fechaInicio,
                    'fecha', '<=', fechaFin).subscribe(res => {
                    this.gastosFijos = res;
                    this.sumaGFijos = 0;
                    this.gastos3 = [];
                    this.gastosFijos.forEach(gfijo => {
                        this.sumaGFijos += gfijo.monto_gasto;
                        this.gastos3.push(gfijo.monto_gasto);
                    });
                    this.sumaGastos();
                });
            }
        });
    }

    // homeComu() {
    //     this.notificationsService.notificacion('home related', 'Se comunica en home');
    // }

    sumaGastos() {
        this.sumaTotalGastos = this.sumaG + this.sumaGFijos;
        this.setLineCharData();
    }

    sumaIngresos() {
        this.sumaTotalIngresos = this.sumaI + this.sumaIfijos;
        this.setLineCharData();
    }

    setLineCharData() {
        this.gastosArray = this.gastos3.concat(this.gastos2);
        this.ingresosArray = this.ingresos3.concat(this.ingresos2)

        this.lineChartData = [
            // {data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A'},
            // {data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B'},
            {data: this.ingresosArray, label: 'Ingreso'},
            {data: this.gastosArray, label: 'Gasto'},
        ];

        let xLabels = 0;
        if (this.ingresosArray.length > this.gastosArray.length) {
            xLabels = this.ingresosArray.length;
        } else {
            xLabels = this.gastosArray.length;
        }
        this.lineChartLabels = [];
        for (let i = 0; i < xLabels; i++) {
            this.lineChartLabels.push(i.toString()); //Mostrar un arreglo de indices aqui
        }
    }


    lineChartData: Array<any> = [
        // {data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A'},
        // {data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B'},
        {data: this.ingresosArray, label: 'Ingreso'},
        {data: this.gastosArray, label: 'Gasto'},
    ];

    public lineChartLabels: Array<any> = [];

    public lineChartOptions: any = {
        responsive: false,
    };

    public lineChartColors: Array<any> = [
        {
            backgroundColor: 'transparent',
            borderColor: 'rgba(41, 191, 118, 1)',
            pointBackgroundColor: 'white',
            pointBorderColor: 'rgba(41, 191, 118, 1)',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(148,159,177,0.8)'
        },
        {
            backgroundColor: 'transparent',
            borderColor: 'rgba(242, 71, 80, 1)',
            pointBackgroundColor: 'white',
            pointBorderColor: 'rgba(242, 71, 80, 1)',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(77,83,96,1)'
        },
    ];

    public lineChartLegend = false;
    public lineChartType = 'line';

    public randomize(): void {
        let _lineChartData: Array<any> = new Array(this.lineChartData.length);
        for (let i = 0; i < this.lineChartData.length; i++) {
            _lineChartData[i] = {data: new Array(this.lineChartData[i].data.length), label: this.lineChartData[i].label};
            for (let j = 0; j < this.lineChartData[i].data.length; j++) {
                _lineChartData[i].data[j] = Math.floor((Math.random() * 100) + 1);
            }
        }
        this.lineChartData = _lineChartData;
    }

    public chartClicked(e: any): void {
        console.log('chartClicked');
    }

    public chartHovered(e: any): void {
        console.log('chartHovered');
    }


}
