import {Router} from '@angular/router';
import {Component, OnInit, Output} from '@angular/core';
import {Categorias, Gasto, Ingreso} from '../models/models.component';
import {NavController} from '@ionic/angular';
import {FirebaseService} from '../../services/firebase.service';
import {AuthenticationService} from '../../services/authentication.service';
import {LocalNotifications} from '@capacitor/local-notifications';
import {InteractionService} from '../../services/interaction.service';
import {NotificationsService} from '../../services/notifications.service';
import {NotificationPage} from '../notification/notification.page';

@Component({
    selector: 'app-home',
    templateUrl: './home.page.html',
    styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

    @Output() mesElegido: number = null;

    porcentaje = 0;
    gastos: Gasto[] = [];
    gastosFijos: Gasto[] = [];
    ingresos: Ingreso[] = [];
    ingresosFijos: Ingreso[] = [];
    sumaG: number = null;
    sumaI: number = null;
    sumaIfijos: number = null;
    sumaGFijos: number = null;
    sumaTotalIngresos: number = null;
    sumaTotalGastos: number = null;
    tabID = null;
    // Suma de elementos de una categoria
    catSuscripcion = null;
    catHogar = null;
    catEducacion = null;
    catComida = null;
    catMascotas = null;
    catEntretenimiento = null;
    catTransporte = null;
    catDeudas = null;
    catOtros = null;
    catCompras;
    exControl = false;
    //Calculo de porcantaje recomendado para cada categoria
    sumaCatHogar = null;
    sumaCatSuscripcion = null;
    sumaCatEntretenimiento = null;
    sumaCatDeudas = null;
    sumaCatComida = null;
    sumaCatMascotas = null;
    sumaCatTransporte = null;
    sumaCatOtros = null;
    sumaCatEducacion = null;
    sumaCatCompras = null;
    montoAhorro = null;

    mesActual = null;
    thisMes: number = null;

    presupuestoExcedido = false;

    categorias: Categorias[] = [];

    ahEx: boolean = false;


    constructor(private navCtrl: NavController,
                public firebaseService: FirebaseService,
                public auth: AuthenticationService,
                public interactionService: InteractionService,
                public router: Router,
                public notificationsService: NotificationsService,
                public notificationPage: NotificationPage
    ) {
        // this.operarMes(this.tabID);  //Talvez haga que se ejecute una orden 2 veces
    }

    async ngOnInit() {   //En logros solo se activa un mes si se hizo clic en un mes desde el home
        await LocalNotifications.requestPermissions();

        const visto = localStorage.getItem('success');
        // console.log('Estado de visto', visto);
        if (visto == 'false') {
            // console.log('visto guia', visto);
            localStorage.setItem('success', 'true');
            await this.router.navigateByUrl('/success');
        } else {
            this.auth.stateUser().subscribe(res => {
                if (res) {
                    const pathIngresos = 'Usuarios/' + res.uid + '/Ingresos';
                    const pathIngresosFijos = 'Usuarios/' + res.uid + '/Ingresos Fijos';
                    const pathGastos = 'Usuarios/' + res.uid + '/Gastos';
                    const pathGastosFijos = 'Usuarios/' + res.uid + '/Gastos Fijos';

                    const fechaActual = new Date();
                    this.tabID = fechaActual.getMonth();
                    this.operarMes(this.tabID);  //Talvez haga que se ejecute una orden 2 veces


                    //if (this.tabID) {
                    //Fecha inicial
                    const fechaInicio = new Date();
                    fechaInicio.setDate(1);
                    fechaInicio.setHours(0);
                    const formatter = new Intl.DateTimeFormat('ec', {month: 'long'});
                    this.mesActual = formatter.format(fechaInicio);
                    //Fecha final
                    const fechaFin = new Date();
                    fechaFin.setDate(30);
                    fechaFin.setHours(23, 59);

                    //Operando el mes actual
                    this.firebaseService.getCollection2Query<Ingreso>(pathIngresos, 'fecha', '>=', fechaInicio,
                        'fecha', '<=', fechaFin).subscribe(res => {
                        this.ingresos = res;
                        this.sumaI = 0;
                        this.ingresos.forEach(ingreso => {
                            this.sumaI += ingreso.monto_ingreso;
                        });
                        this.sumaIngresos();
                    });

                    this.firebaseService.getCollection2Query<Ingreso>(pathIngresosFijos, 'fecha', '>=', fechaInicio,
                        'fecha', '<=', fechaFin).subscribe(res => {
                        this.ingresosFijos = res;
                        this.sumaIfijos = 0;
                        this.ingresosFijos.forEach(ifijo => {
                            this.sumaIfijos += ifijo.monto_ingreso;
                        });
                        this.sumaIngresos();
                    });

                    this.firebaseService.getCollection2Query<Gasto>(pathGastos, 'fecha', '>=', fechaInicio,
                        'fecha', '<=', fechaFin).subscribe(res => {
                        this.gastos = res;
                        this.sumaG = 0;
                        this.catEducacion = 0;
                        this.catOtros = 0;
                        this.catEntretenimiento = 0;
                        this.catSuscripcion = 0;
                        this.catDeudas = 0;
                        this.catComida = 0;
                        this.catMascotas = 0;
                        this.catTransporte = 0;
                        this.catHogar = 0;
                        this.catCompras = 0;
                        this.gastos.forEach(gasto => {
                            this.sumaG += gasto.monto_gasto;
                            if (gasto.categoria == 'Educación') {
                                this.catEducacion += gasto.monto_gasto;
                                this.exControl = true;
                            }
                            if (gasto.categoria == 'Hogar') {
                                this.catHogar += gasto.monto_gasto;
                                this.exControl = true;

                                // this.sumaCatHogar = 0;
                                // this.sumaCatHogar = (this.sumaTotalIngresos * 0.15).toFixed(2);
                                // console.log('sumaCatHogar', this.sumaCatHogar);
                            }

                            if (gasto.categoria == 'Suscripción') {
                                this.catSuscripcion += gasto.monto_gasto;
                                this.exControl = true;
                            }
                            if (gasto.categoria == 'Comida') {
                                this.catComida += gasto.monto_gasto;
                                this.exControl = true;
                            }
                            if (gasto.categoria == 'Mascotas') {
                                this.catMascotas += gasto.monto_gasto;
                                this.exControl = true;
                            }
                            if (gasto.categoria == 'Entretenimiento') {
                                this.catEntretenimiento += gasto.monto_gasto;
                                this.exControl = true;
                            }
                            if (gasto.categoria == 'Transporte') {
                                this.catTransporte += gasto.monto_gasto;
                                this.exControl = true;
                            }
                            if (gasto.categoria == 'Deudas') {
                                this.catDeudas += gasto.monto_gasto;
                                this.exControl = true;
                            }
                            if (gasto.categoria == 'Compras') {
                                this.catCompras += gasto.monto_gasto;
                                this.exControl = true;
                            }
                            if (gasto.categoria == 'Otros') {
                                this.catOtros += gasto.monto_gasto;
                                this.exControl = true;
                            }
                        });
                        this.sumaGastos();
                    });
                    //}
                    this.firebaseService.getCollection2Query<Gasto>(pathGastosFijos, 'fecha', '>=', fechaInicio,
                        'fecha', '<=', fechaFin).subscribe(res => {
                        this.gastosFijos = res;
                        this.sumaGFijos = 0;
                        this.gastosFijos.forEach(gFijo => {
                            this.sumaGFijos += gFijo.monto_gasto;
                        });
                        // console.log('this.gastosFijos', this.gastosFijos);
                        // console.log('this.sumaGFijos', this.sumaGFijos);
                        this.sumaGastos();
                    });
                }
            });
        }
    }

    async operarMes(mes: number) {
        this.thisMes = mes;
        this.interactionService.mesElegido = this.thisMes;
        this.interactionService.presentToast('Cargando datos...');
        //if (this.mesEnero != this.tabID) {  //Si no es enero, trae lo de enero
        this.auth.stateUser().subscribe(res => {
            if (res) {
                this.interactionService.closeLoading();
                const fechaActual = new Date();

                const pathIngresos = 'Usuarios/' + res.uid + '/Ingresos';
                const pathIngresosFijos = 'Usuarios/' + res.uid + '/Ingresos Fijos';
                const pathGastos = 'Usuarios/' + res.uid + '/Gastos';
                const pathGastosFIjos = 'Usuarios/' + res.uid + '/Gastos Fijos';

                //Fecha inicio
                const fechaInicio = new Date();
                fechaInicio.setMonth(mes, 1);
                fechaInicio.setHours(0, 0, 0);
                const formatter = new Intl.DateTimeFormat('ec', {month: 'long'});
                const month1 = formatter.format(fechaInicio);
                this.mesActual = month1;
                // console.log('Mes actual', fechaInicio);

                //Fecha final
                const fechaFin = new Date();
                fechaFin.setMonth(mes, 30);
                fechaFin.setHours(23, 59, 59);

                this.firebaseService.getCollection2Query<Ingreso>(pathIngresos, 'fecha', '>=', fechaInicio,
                    'fecha', '<=', fechaFin).subscribe(res => {
                    this.ingresos = res;
                    this.sumaI = 0;
                    this.ingresos.forEach(ingreso => {
                        this.sumaI += ingreso.monto_ingreso;
                    });
                    this.sumaIngresos();
                });

                this.firebaseService.getCollection2Query<Ingreso>(pathIngresosFijos, 'fecha', '>=', fechaInicio,
                    'fecha', '<=', fechaFin).subscribe(res => {
                    this.ingresosFijos = res;
                    this.sumaIfijos = 0;
                    this.ingresosFijos.forEach(ifijo => {
                        this.sumaIfijos += ifijo.monto_ingreso;
                    });
                    this.sumaIngresos();
                });

                this.firebaseService.getCollection2Query<Gasto>(pathGastos, 'fecha', '>=', fechaInicio,
                    'fecha', '<=', fechaFin).subscribe(res => {
                    this.gastos = res;
                    this.sumaG = 0;
                    this.catEducacion = 0;
                    this.catOtros = 0;
                    this.catEntretenimiento = 0;
                    this.catSuscripcion = 0;
                    this.catDeudas = 0;
                    this.catComida = 0;
                    this.catMascotas = 0;
                    this.catTransporte = 0;
                    this.catHogar = 0;
                    this.catCompras = 0;
                    // Presupuesto recomendado por categoria

                    this.gastos.forEach(gasto => {
                        this.sumaG += gasto.monto_gasto;
                        if (gasto.categoria == 'Educación') {
                            this.catEducacion += gasto.monto_gasto;
                            this.exControl = true;
                        }
                        if (gasto.categoria == 'Hogar') {
                            this.catHogar += gasto.monto_gasto;
                            this.exControl = true;
                        }
                        if (gasto.categoria == 'Suscripción') {
                            this.catSuscripcion += gasto.monto_gasto;
                            this.exControl = true;
                        }
                        if (gasto.categoria == 'Comida') {
                            this.catComida += gasto.monto_gasto;
                            this.exControl = true;
                        }
                        if (gasto.categoria == 'Mascotas') {
                            this.catMascotas += gasto.monto_gasto;
                            this.exControl = true;
                        }
                        if (gasto.categoria == 'Entretenimiento') {
                            this.catEntretenimiento += gasto.monto_gasto;
                            this.exControl = true;
                        }
                        if (gasto.categoria == 'Transporte') {
                            this.catTransporte += gasto.monto_gasto;
                            this.exControl = true;
                        }
                        if (gasto.categoria == 'Deudas') {
                            this.catDeudas += gasto.monto_gasto;
                            this.exControl = true;
                        }
                        if (gasto.categoria == 'Compras') {
                            this.catCompras += gasto.monto_gasto;
                            this.exControl = true;
                        }
                        if (gasto.categoria == 'Otros') {
                            this.catOtros += gasto.monto_gasto;
                            this.exControl = true;
                        }
                    });
                    this.sumaGastos();
                });
                this.firebaseService.getCollection2Query<Gasto>(pathGastosFIjos, 'fecha', '>=', fechaInicio,
                    'fecha', '<=', fechaFin).subscribe(res => {
                    this.gastosFijos = res;
                    this.sumaGFijos = 0;
                    this.gastosFijos.forEach(gasto => {
                        this.sumaGFijos += gasto.monto_gasto;
                    });
                    // console.log('Suma de gastos fijos', this.sumaGFijos);
                    this.sumaGastos();
                });
            }
        });
    }

    // async getGasto() {   Operaciones con gasto fijo solo deben tener el get sin consulta de fecha
    //     const uid = await this.auth.getUid();
    //     const ruta = 'Usuarios/' + uid + '/Gastos Fijos';
    //     this.firebaseService.getCollection<Gasto>(ruta).subscribe(res => {
    //         this.gastos = res;
    //         console.log('los gastos son ', this.gastos, ' ', ruta);
    //     });
    // }

    sumaGastos() {
        this.sumaTotalGastos = this.sumaG + this.sumaGFijos;
        // console.log('Suma total de gastos', this.sumaTotalGastos);
        this.getPorcentaje();
        this.getCategorias();
    }

    sumaIngresos() {
        this.sumaTotalIngresos = this.sumaI + this.sumaIfijos;
        console.log('Suma total de ingresos es:', this.sumaTotalIngresos);
        this.getPorcentaje();
        if (this.sumaTotalIngresos > 0) {
            this.env1();
        }
    }

    getPorcentaje() {
        this.porcentaje = this.sumaTotalGastos / this.sumaTotalIngresos;
        let finito = Number.isFinite(this.sumaTotalGastos / this.sumaTotalIngresos);
        if (finito == false) {
            this.porcentaje = 0;
        }
        if (this.porcentaje > 1 && this.sumaTotalGastos != 0 && this.sumaTotalIngresos != 0) {
            //const suceso = new Date().toLocaleString();
            //this.notificationsService.guardarNotification('Presupuesto general excedido', 'presupuesto', true, suceso);
            this.notificationsService.notificacionPresupuesto = true;
            this.interactionService.notificacion('Precaución', 'Se ha rebasado el presupuesto general');
            this.presupuestoExcedido = true;
        }
    }

    env1() {
        this.sumaCatHogar = 0;
        this.sumaCatCompras = 0;
        this.sumaCatSuscripcion = 0;
        this.sumaCatTransporte = 0;
        this.sumaCatComida = 0;
        this.sumaCatMascotas = 0;
        this.sumaCatEntretenimiento = 0;
        this.sumaCatEducacion = 0;
        this.sumaCatDeudas = 0;
        this.sumaCatOtros = 0;
        this.montoAhorro = 0;

        this.categorias.forEach(i => {

            // Seteo de valores
            if (i.hogar == true && i.compras == true && i.comida == true && i.otros == true &&
                i.mascotas == true && i.deudas == true && i.suscripcion == true && i.transporte == true &&
                i.educacion == true && i.entretenimiento == true) {
                this.sumaCatHogar = (this.sumaTotalIngresos * 0.15).toFixed(2);
                this.sumaCatCompras = (this.sumaTotalIngresos * 0.06).toFixed(2);
                this.sumaCatComida = (this.sumaTotalIngresos * 0.28).toFixed(2);
                this.sumaCatOtros = (this.sumaTotalIngresos * 0.05).toFixed(2);
                this.sumaCatMascotas = (this.sumaTotalIngresos * 0.05).toFixed(2);
                this.sumaCatDeudas = (this.sumaTotalIngresos * 0.05).toFixed(2);
                this.sumaCatSuscripcion = (this.sumaTotalIngresos * 0.04).toFixed(2);
                this.sumaCatTransporte = (this.sumaTotalIngresos * 0.08).toFixed(2);
                this.sumaCatEducacion = (this.sumaTotalIngresos * 0.05).toFixed(2);
                this.sumaCatEntretenimiento = (this.sumaTotalIngresos * 0.05).toFixed(2);
                this.ahorro();
            }

            // De hogar a compras
            if (i.hogar == false && i.compras == true && i.comida == true && i.otros == true &&
                i.mascotas == true && i.deudas == true && i.suscripcion == true && i.transporte == true &&
                i.educacion == true && i.entretenimiento == true) {
                this.sumaCatCompras = (this.sumaTotalIngresos * 0.21).toFixed(2);
                this.sumaCatComida = (this.sumaTotalIngresos * 0.28).toFixed(2);
                this.sumaCatOtros = (this.sumaTotalIngresos * 0.05).toFixed(2);
                this.sumaCatMascotas = (this.sumaTotalIngresos * 0.05).toFixed(2);
                this.sumaCatDeudas = (this.sumaTotalIngresos * 0.05).toFixed(2);
                this.sumaCatSuscripcion = (this.sumaTotalIngresos * 0.04).toFixed(2);
                this.sumaCatTransporte = (this.sumaTotalIngresos * 0.08).toFixed(2);
                this.sumaCatEducacion = (this.sumaTotalIngresos * 0.05).toFixed(2);
                this.sumaCatEntretenimiento = (this.sumaTotalIngresos * 0.05).toFixed(2);
            }

            // De compras a comida
            if (i.hogar == true && i.compras == false && i.comida == true && i.otros == true &&
                i.mascotas == true && i.deudas == true && i.suscripcion == true && i.transporte == true &&
                i.educacion == true && i.entretenimiento == true) {
                this.sumaCatHogar = (this.sumaTotalIngresos * 0.15).toFixed(2);
                this.sumaCatComida = (this.sumaTotalIngresos * 0.34).toFixed(2);
                this.sumaCatOtros = (this.sumaTotalIngresos * 0.05).toFixed(2);
                this.sumaCatMascotas = (this.sumaTotalIngresos * 0.05).toFixed(2);
                this.sumaCatDeudas = (this.sumaTotalIngresos * 0.05).toFixed(2);
                this.sumaCatSuscripcion = (this.sumaTotalIngresos * 0.04).toFixed(2);
                this.sumaCatTransporte = (this.sumaTotalIngresos * 0.08).toFixed(2);
                this.sumaCatEducacion = (this.sumaTotalIngresos * 0.05).toFixed(2);
                this.sumaCatEntretenimiento = (this.sumaTotalIngresos * 0.05).toFixed(2);
            }

            // De comida a otros
            if (i.hogar == true && i.compras == true && i.comida == false && i.otros == true &&
                i.mascotas == true && i.deudas == true && i.suscripcion == true && i.transporte == true &&
                i.educacion == true && i.entretenimiento == true) {
                this.sumaCatHogar = (this.sumaTotalIngresos * 0.15).toFixed(2);
                this.sumaCatCompras = (this.sumaTotalIngresos * 0.06).toFixed(2);
                this.sumaCatOtros = (this.sumaTotalIngresos * 0.33).toFixed(2);
                this.sumaCatMascotas = (this.sumaTotalIngresos * 0.05).toFixed(2);
                this.sumaCatDeudas = (this.sumaTotalIngresos * 0.05).toFixed(2);
                this.sumaCatSuscripcion = (this.sumaTotalIngresos * 0.04).toFixed(2);
                this.sumaCatTransporte = (this.sumaTotalIngresos * 0.08).toFixed(2);
                this.sumaCatEducacion = (this.sumaTotalIngresos * 0.05).toFixed(2);
                this.sumaCatEntretenimiento = (this.sumaTotalIngresos * 0.05).toFixed(2);
            }

            // De otros a mascotas
            if (i.hogar == true && i.compras == true && i.comida == true && i.otros == false &&
                i.mascotas == true && i.deudas == true && i.suscripcion == true && i.transporte == true &&
                i.educacion == true && i.entretenimiento == true) {
                this.sumaCatHogar = (this.sumaTotalIngresos * 0.15).toFixed(2);
                this.sumaCatCompras = (this.sumaTotalIngresos * 0.06).toFixed(2);
                this.sumaCatComida = (this.sumaTotalIngresos * 0.28).toFixed(2);
                this.sumaCatMascotas = (this.sumaTotalIngresos * 0.10).toFixed(2);
                this.sumaCatDeudas = (this.sumaTotalIngresos * 0.05).toFixed(2);
                this.sumaCatSuscripcion = (this.sumaTotalIngresos * 0.04).toFixed(2);
                this.sumaCatTransporte = (this.sumaTotalIngresos * 0.08).toFixed(2);
                this.sumaCatEducacion = (this.sumaTotalIngresos * 0.05).toFixed(2);
                this.sumaCatEntretenimiento = (this.sumaTotalIngresos * 0.05).toFixed(2);
            }

            // De mascotas a deudas
            if (i.hogar == true && i.compras == true && i.comida == true && i.otros == true &&
                i.mascotas == false && i.deudas == true && i.suscripcion == true && i.transporte == true &&
                i.educacion == true && i.entretenimiento == true) {
                this.sumaCatHogar = (this.sumaTotalIngresos * 0.15).toFixed(2);
                this.sumaCatCompras = (this.sumaTotalIngresos * 0.06).toFixed(2);
                this.sumaCatComida = (this.sumaTotalIngresos * 0.28).toFixed(2);
                this.sumaCatOtros = (this.sumaTotalIngresos * 0.05).toFixed(2);
                this.sumaCatDeudas = (this.sumaTotalIngresos * 0.10).toFixed(2);
                this.sumaCatSuscripcion = (this.sumaTotalIngresos * 0.04).toFixed(2);
                this.sumaCatTransporte = (this.sumaTotalIngresos * 0.08).toFixed(2);
                this.sumaCatEducacion = (this.sumaTotalIngresos * 0.05).toFixed(2);
                this.sumaCatEntretenimiento = (this.sumaTotalIngresos * 0.05).toFixed(2);
            }

            // De deudas a hogar
            if (i.hogar == true && i.compras == true && i.comida == true && i.otros == true &&
                i.mascotas == true && i.deudas == false && i.suscripcion == true && i.transporte == true &&
                i.educacion == true && i.entretenimiento == true) {
                this.sumaCatHogar = (this.sumaTotalIngresos * 0.20).toFixed(2);
                this.sumaCatCompras = (this.sumaTotalIngresos * 0.06).toFixed(2);
                this.sumaCatComida = (this.sumaTotalIngresos * 0.28).toFixed(2);
                this.sumaCatOtros = (this.sumaTotalIngresos * 0.05).toFixed(2);
                this.sumaCatSuscripcion = (this.sumaTotalIngresos * 0.04).toFixed(2);
                this.sumaCatMascotas = (this.sumaTotalIngresos * 0.05).toFixed(2);
                this.sumaCatTransporte = (this.sumaTotalIngresos * 0.08).toFixed(2);
                this.sumaCatEducacion = (this.sumaTotalIngresos * 0.05).toFixed(2);
                this.sumaCatEntretenimiento = (this.sumaTotalIngresos * 0.05).toFixed(2);
                this.ahorro();
            }

            // De suscripcion a transporte
            if (i.hogar == true && i.compras == true && i.comida == true && i.otros == true &&
                i.mascotas == true && i.deudas == true && i.suscripcion == false && i.transporte == true &&
                i.educacion == true && i.entretenimiento == true) {
                this.sumaCatHogar = (this.sumaTotalIngresos * 0.15).toFixed(2);
                this.sumaCatCompras = (this.sumaTotalIngresos * 0.06).toFixed(2);
                this.sumaCatComida = (this.sumaTotalIngresos * 0.28).toFixed(2);
                this.sumaCatOtros = (this.sumaTotalIngresos * 0.05).toFixed(2);
                this.sumaCatMascotas = (this.sumaTotalIngresos * 0.05).toFixed(2);
                this.sumaCatDeudas = (this.sumaTotalIngresos * 0.05).toFixed(2);
                this.sumaCatTransporte = (this.sumaTotalIngresos * 0.12).toFixed(2);
                this.sumaCatEducacion = (this.sumaTotalIngresos * 0.05).toFixed(2);
                this.sumaCatEntretenimiento = (this.sumaTotalIngresos * 0.05).toFixed(2);
            }

            // De transporte a suscripcion
            if (i.hogar == true && i.compras == true && i.comida == true && i.otros == true &&
                i.mascotas == true && i.deudas == true && i.suscripcion == true && i.transporte == false &&
                i.educacion == true && i.entretenimiento == true) {
                this.sumaCatHogar = (this.sumaTotalIngresos * 0.15).toFixed(2);
                this.sumaCatCompras = (this.sumaTotalIngresos * 0.06).toFixed(2);
                this.sumaCatComida = (this.sumaTotalIngresos * 0.28).toFixed(2);
                this.sumaCatOtros = (this.sumaTotalIngresos * 0.05).toFixed(2);
                this.sumaCatMascotas = (this.sumaTotalIngresos * 0.05).toFixed(2);
                this.sumaCatDeudas = (this.sumaTotalIngresos * 0.05).toFixed(2);
                this.sumaCatSuscripcion = (this.sumaTotalIngresos * 0.12).toFixed(2);
                this.sumaCatEducacion = (this.sumaTotalIngresos * 0.05).toFixed(2);
                this.sumaCatEntretenimiento = (this.sumaTotalIngresos * 0.05).toFixed(2);
            }

            // De educacion a entrerenimiento
            if (i.hogar == true && i.compras == true && i.comida == true && i.otros == true &&
                i.mascotas == true && i.deudas == true && i.suscripcion == true && i.transporte == true &&
                i.educacion == false && i.entretenimiento == true) {
                this.sumaCatHogar = (this.sumaTotalIngresos * 0.15).toFixed(2);
                this.sumaCatCompras = (this.sumaTotalIngresos * 0.06).toFixed(2);
                this.sumaCatComida = (this.sumaTotalIngresos * 0.28).toFixed(2);
                this.sumaCatOtros = (this.sumaTotalIngresos * 0.05).toFixed(2);
                this.sumaCatDeudas = (this.sumaTotalIngresos * 0.05).toFixed(2);
                this.sumaCatSuscripcion = (this.sumaTotalIngresos * 0.04).toFixed(2);
                this.sumaCatTransporte = (this.sumaTotalIngresos * 0.08).toFixed(2);
                this.sumaCatMascotas = (this.sumaTotalIngresos * 0.05).toFixed(2);
                this.sumaCatEntretenimiento = (this.sumaTotalIngresos * 0.10).toFixed(2);
            }

            // De entretenimiento a educación
            if (i.hogar == true && i.compras == true && i.comida == true && i.otros == true &&
                i.mascotas == true && i.deudas == true && i.suscripcion == true && i.transporte == true &&
                i.educacion == true && i.entretenimiento == false) {
                this.sumaCatHogar = (this.sumaTotalIngresos * 0.15).toFixed(2);
                this.sumaCatCompras = (this.sumaTotalIngresos * 0.06).toFixed(2);
                this.sumaCatComida = (this.sumaTotalIngresos * 0.28).toFixed(2);
                this.sumaCatOtros = (this.sumaTotalIngresos * 0.05).toFixed(2);
                this.sumaCatMascotas = (this.sumaTotalIngresos * 0.05).toFixed(2);
                this.sumaCatDeudas = (this.sumaTotalIngresos * 0.05).toFixed(2);
                this.sumaCatSuscripcion = (this.sumaTotalIngresos * 0.04).toFixed(2);
                this.sumaCatTransporte = (this.sumaTotalIngresos * 0.08).toFixed(2);
                this.sumaCatEducacion = (this.sumaTotalIngresos * 0.10).toFixed(2);
            }
        });
    }

    ahorro() {
        this.montoAhorro = 0;
        this.montoAhorro = (this.sumaTotalIngresos * 0.10).toFixed(2);
        if (this.sumaTotalGastos - (this.sumaTotalIngresos * 0.10) > this.sumaTotalIngresos - this.sumaTotalIngresos * 0.10) {
            this.ahEx = true;
        } else {
            this.ahEx = false;
        }

        // this.categorias.forEach( i => {
        //     if (i.deudas == false){
        //         this.montoAhorro = (this.sumaTotalIngresos * 0.10).toFixed(2);
        //     }
        // })
    }

    // prueber() {
    //     this.sumaCatHogar = 0;
    //     this.sumaCatCompras = 0;
    //     this.sumaCatSuscripcion = 0;
    //     this.sumaCatTransporte = 0;
    //     this.sumaCatComida = 0;
    //     this.sumaCatMascotas = 0;
    //     this.sumaCatEntretenimiento = 0;
    //     this.sumaCatEducacion = 0;
    //     this.sumaCatDeudas = 0;
    //     this.sumaCatOtros = 0;
    //     this.montoAhorro = 0;
    //
    //     this.categorias.forEach(i => {
    //         // Se verifica si todos los valores estan activados
    //         switch (i.hogar == true && i.compras == true && i.comida == true && i.otros == true &&
    //         i.mascotas == true && i.deudas == true && i.suscripcion == true && i.transporte == true &&
    //         i.educacion == true && i.entretenimiento == true) {
    //             // En caso de que todos los valores esten activados, se asigna normalmente el valor
    //             // recomendado de gasto para la categoria.
    //             default:
    //                 this.sumaCatHogar = (this.sumaTotalIngresos * 0.15).toFixed(2);
    //                 this.sumaCatCompras = (this.sumaTotalIngresos * 0.06).toFixed(2);
    //                 this.sumaCatComida = (this.sumaTotalIngresos * 0.28).toFixed(2);
    //                 this.sumaCatOtros = (this.sumaTotalIngresos * 0.05).toFixed(2);
    //                 this.sumaCatMascotas = (this.sumaTotalIngresos * 0.05).toFixed(2);
    //                 this.sumaCatDeudas = (this.sumaTotalIngresos * 0.05).toFixed(2);
    //                 this.sumaCatSuscripcion = (this.sumaTotalIngresos * 0.04).toFixed(2);
    //                 this.sumaCatTransporte = (this.sumaTotalIngresos * 0.08).toFixed(2);
    //                 this.sumaCatEducacion = (this.sumaTotalIngresos * 0.05).toFixed(2);
    //                 this.sumaCatEntretenimiento = (this.sumaTotalIngresos * 0.05).toFixed(2);
    //                 this.montoAhorro = (this.sumaTotalIngresos * 0.10).toFixed(2);
    //
    //             // En este caso hogar es false, por tanto su porcentaje se suma con "categoria compras"
    //             // para que no se pierda.
    //
    //             // De hogar a compras
    //             case (i.hogar == false && i.compras == true && i.comida == true && i.otros == true &&
    //                 i.mascotas == true && i.deudas == true && i.suscripcion == true && i.transporte == true &&
    //                 i.educacion == true && i.entretenimiento == true):
    //                 this.sumaCatCompras = (this.sumaTotalIngresos * 0.21).toFixed(2);
    //                 this.sumaCatComida = (this.sumaTotalIngresos * 0.28).toFixed(2);
    //                 this.sumaCatOtros = (this.sumaTotalIngresos * 0.05).toFixed(2);
    //                 this.sumaCatMascotas = (this.sumaTotalIngresos * 0.05).toFixed(2);
    //                 this.sumaCatDeudas = (this.sumaTotalIngresos * 0.05).toFixed(2);
    //                 this.sumaCatSuscripcion = (this.sumaTotalIngresos * 0.04).toFixed(2);
    //                 this.sumaCatTransporte = (this.sumaTotalIngresos * 0.08).toFixed(2);
    //                 this.sumaCatEducacion = (this.sumaTotalIngresos * 0.05).toFixed(2);
    //                 this.sumaCatEntretenimiento = (this.sumaTotalIngresos * 0.05).toFixed(2);
    //                 break;
    //
    //             //De compras a comida
    //             case (i.hogar == true && i.compras == false && i.comida == true && i.otros == true &&
    //                 i.mascotas == true && i.deudas == true && i.suscripcion == true && i.transporte == true &&
    //                 i.educacion == true && i.entretenimiento == true):
    //                 this.sumaCatHogar = (this.sumaTotalIngresos * 0.15).toFixed(2);
    //                 this.sumaCatComida = (this.sumaTotalIngresos * 0.34).toFixed(2);
    //                 this.sumaCatOtros = (this.sumaTotalIngresos * 0.05).toFixed(2);
    //                 this.sumaCatMascotas = (this.sumaTotalIngresos * 0.05).toFixed(2);
    //                 this.sumaCatDeudas = (this.sumaTotalIngresos * 0.05).toFixed(2);
    //                 this.sumaCatSuscripcion = (this.sumaTotalIngresos * 0.04).toFixed(2);
    //                 this.sumaCatTransporte = (this.sumaTotalIngresos * 0.08).toFixed(2);
    //                 this.sumaCatEducacion = (this.sumaTotalIngresos * 0.05).toFixed(2);
    //                 this.sumaCatEntretenimiento = (this.sumaTotalIngresos * 0.05).toFixed(2);
    //                 break;
    //
    //             // De comida a otros
    //             case (i.hogar == true && i.compras == true && i.comida == false && i.otros == true &&
    //                 i.mascotas == true && i.deudas == true && i.suscripcion == true && i.transporte == true &&
    //                 i.educacion == true && i.entretenimiento == true):
    //                 this.sumaCatHogar = (this.sumaTotalIngresos * 0.15).toFixed(2);
    //                 this.sumaCatCompras = (this.sumaTotalIngresos * 0.06).toFixed(2);
    //                 this.sumaCatOtros = (this.sumaTotalIngresos * 0.33).toFixed(2);
    //                 this.sumaCatMascotas = (this.sumaTotalIngresos * 0.05).toFixed(2);
    //                 this.sumaCatDeudas = (this.sumaTotalIngresos * 0.05).toFixed(2);
    //                 this.sumaCatSuscripcion = (this.sumaTotalIngresos * 0.04).toFixed(2);
    //                 this.sumaCatTransporte = (this.sumaTotalIngresos * 0.08).toFixed(2);
    //                 this.sumaCatEducacion = (this.sumaTotalIngresos * 0.05).toFixed(2);
    //                 this.sumaCatEntretenimiento = (this.sumaTotalIngresos * 0.05).toFixed(2);
    //                 break;
    //
    //             //  De otros a mascotas
    //             case (i.hogar == true && i.compras == true && i.comida == true && i.otros == false &&
    //                 i.mascotas == true && i.deudas == true && i.suscripcion == true && i.transporte == true &&
    //                 i.educacion == true && i.entretenimiento == true):
    //                 this.sumaCatHogar = (this.sumaTotalIngresos * 0.15).toFixed(2);
    //                 this.sumaCatCompras = (this.sumaTotalIngresos * 0.06).toFixed(2);
    //                 this.sumaCatComida = (this.sumaTotalIngresos * 0.28).toFixed(2);
    //                 this.sumaCatMascotas = (this.sumaTotalIngresos * 0.10).toFixed(2);
    //                 this.sumaCatDeudas = (this.sumaTotalIngresos * 0.05).toFixed(2);
    //                 this.sumaCatSuscripcion = (this.sumaTotalIngresos * 0.04).toFixed(2);
    //                 this.sumaCatTransporte = (this.sumaTotalIngresos * 0.08).toFixed(2);
    //                 this.sumaCatEducacion = (this.sumaTotalIngresos * 0.05).toFixed(2);
    //                 this.sumaCatEntretenimiento = (this.sumaTotalIngresos * 0.05).toFixed(2);
    //                 break;
    //
    //             // De mascotas a deudas
    //             case (i.hogar == true && i.compras == true && i.comida == true && i.otros == true &&
    //                 i.mascotas == false && i.deudas == true && i.suscripcion == true && i.transporte == true &&
    //                 i.educacion == true && i.entretenimiento == true):
    //                 this.sumaCatHogar = (this.sumaTotalIngresos * 0.15).toFixed(2);
    //                 this.sumaCatCompras = (this.sumaTotalIngresos * 0.06).toFixed(2);
    //                 this.sumaCatComida = (this.sumaTotalIngresos * 0.28).toFixed(2);
    //                 this.sumaCatOtros = (this.sumaTotalIngresos * 0.05).toFixed(2);
    //                 this.sumaCatDeudas = (this.sumaTotalIngresos * 0.10).toFixed(2);
    //                 this.sumaCatSuscripcion = (this.sumaTotalIngresos * 0.04).toFixed(2);
    //                 this.sumaCatTransporte = (this.sumaTotalIngresos * 0.08).toFixed(2);
    //                 this.sumaCatEducacion = (this.sumaTotalIngresos * 0.05).toFixed(2);
    //                 this.sumaCatEntretenimiento = (this.sumaTotalIngresos * 0.05).toFixed(2);
    //                 break;
    //
    //             // De deudas a hogar
    //             case (i.hogar == true && i.compras == true && i.comida == true && i.otros == true &&
    //                 i.mascotas == true && i.deudas == false && i.suscripcion == true && i.transporte == true &&
    //                 i.educacion == true && i.entretenimiento == true):
    //                 this.sumaCatHogar = (this.sumaTotalIngresos * 0.20).toFixed(2);
    //                 this.sumaCatCompras = (this.sumaTotalIngresos * 0.06).toFixed(2);
    //                 this.sumaCatComida = (this.sumaTotalIngresos * 0.28).toFixed(2);
    //                 this.sumaCatOtros = (this.sumaTotalIngresos * 0.05).toFixed(2);
    //                 this.sumaCatSuscripcion = (this.sumaTotalIngresos * 0.04).toFixed(2);
    //                 this.sumaCatMascotas = (this.sumaTotalIngresos * 0.05).toFixed(2);
    //                 this.sumaCatTransporte = (this.sumaTotalIngresos * 0.08).toFixed(2);
    //                 this.sumaCatEducacion = (this.sumaTotalIngresos * 0.05).toFixed(2);
    //                 this.sumaCatEntretenimiento = (this.sumaTotalIngresos * 0.05).toFixed(2);
    //                 break;
    //
    //             // De suscripcion a transporte
    //             case (i.hogar == true && i.compras == true && i.comida == true && i.otros == true &&
    //                 i.mascotas == true && i.deudas == true && i.suscripcion == false && i.transporte == true &&
    //                 i.educacion == true && i.entretenimiento == true):
    //                 this.sumaCatHogar = (this.sumaTotalIngresos * 0.15).toFixed(2);
    //                 this.sumaCatCompras = (this.sumaTotalIngresos * 0.06).toFixed(2);
    //                 this.sumaCatComida = (this.sumaTotalIngresos * 0.28).toFixed(2);
    //                 this.sumaCatOtros = (this.sumaTotalIngresos * 0.05).toFixed(2);
    //                 this.sumaCatDeudas = (this.sumaTotalIngresos * 0.05).toFixed(2);
    //                 this.sumaCatMascotas = (this.sumaTotalIngresos * 0.05).toFixed(2);
    //                 this.sumaCatTransporte = (this.sumaTotalIngresos * 0.12).toFixed(2);
    //                 this.sumaCatEducacion = (this.sumaTotalIngresos * 0.05).toFixed(2);
    //                 this.sumaCatEntretenimiento = (this.sumaTotalIngresos * 0.05).toFixed(2);
    //                 break;
    //
    //             // De transporte a suscripcion
    //             case (i.hogar == true && i.compras == true && i.comida == true && i.otros == true &&
    //                 i.mascotas == true && i.deudas == true && i.suscripcion == true && i.transporte == false &&
    //                 i.educacion == true && i.entretenimiento == true):
    //                 this.sumaCatHogar = (this.sumaTotalIngresos * 0.15).toFixed(2);
    //                 this.sumaCatCompras = (this.sumaTotalIngresos * 0.06).toFixed(2);
    //                 this.sumaCatComida = (this.sumaTotalIngresos * 0.28).toFixed(2);
    //                 this.sumaCatOtros = (this.sumaTotalIngresos * 0.05).toFixed(2);
    //                 this.sumaCatSuscripcion = (this.sumaTotalIngresos * 0.12).toFixed(2);
    //                 this.sumaCatMascotas = (this.sumaTotalIngresos * 0.05).toFixed(2);
    //                 this.sumaCatDeudas = (this.sumaTotalIngresos * 0.05).toFixed(2);
    //                 this.sumaCatEducacion = (this.sumaTotalIngresos * 0.05).toFixed(2);
    //                 this.sumaCatEntretenimiento = (this.sumaTotalIngresos * 0.05).toFixed(2);
    //                 break;
    //
    //             // De educacion a entretenimiento
    //             case (i.hogar == true && i.compras == true && i.comida == true && i.otros == true &&
    //                 i.mascotas == true && i.deudas == true && i.suscripcion == true && i.transporte == true &&
    //                 i.educacion == false && i.entretenimiento == true):
    //                 this.sumaCatHogar = (this.sumaTotalIngresos * 0.15).toFixed(2);
    //                 this.sumaCatCompras = (this.sumaTotalIngresos * 0.06).toFixed(2);
    //                 this.sumaCatComida = (this.sumaTotalIngresos * 0.28).toFixed(2);
    //                 this.sumaCatOtros = (this.sumaTotalIngresos * 0.05).toFixed(2);
    //                 this.sumaCatSuscripcion = (this.sumaTotalIngresos * 0.12).toFixed(2);
    //                 this.sumaCatDeudas = (this.sumaTotalIngresos * 0.05).toFixed(2);
    //                 this.sumaCatTransporte = (this.sumaTotalIngresos * 0.08).toFixed(2);
    //                 this.sumaCatEntretenimiento = (this.sumaTotalIngresos * 0.10).toFixed(2);
    //                 this.sumaCatMascotas = (this.sumaTotalIngresos * 0.05).toFixed(2);
    //                 break;
    //
    //             // De entretenimiento a educacion
    //             case (i.hogar == true && i.compras == true && i.comida == true && i.otros == true &&
    //                 i.mascotas == true && i.deudas == true && i.suscripcion == true && i.transporte == true &&
    //                 i.educacion == true && i.entretenimiento == false):
    //                 this.sumaCatHogar = (this.sumaTotalIngresos * 0.15).toFixed(2);
    //                 this.sumaCatCompras = (this.sumaTotalIngresos * 0.06).toFixed(2);
    //                 this.sumaCatComida = (this.sumaTotalIngresos * 0.28).toFixed(2);
    //                 this.sumaCatOtros = (this.sumaTotalIngresos * 0.05).toFixed(2);
    //                 this.sumaCatSuscripcion = (this.sumaTotalIngresos * 0.12).toFixed(2);
    //                 this.sumaCatEducacion = (this.sumaTotalIngresos * 0.10).toFixed(2);
    //                 this.sumaCatDeudas = (this.sumaTotalIngresos * 0.05).toFixed(2);
    //                 this.sumaCatTransporte = (this.sumaTotalIngresos * 0.08).toFixed(2);
    //                 this.sumaCatMascotas = (this.sumaTotalIngresos * 0.05).toFixed(2);
    //                 break;
    //         }
    //     });
    // }


    async getCategorias() {
        const uid = await this.auth.getUid();
        const ruta = 'Usuarios/' + uid + '/Categorias Activas';
        this.categorias = [];
        this.firebaseService.getCollection<Categorias>(ruta).subscribe(res => {
            this.categorias = res;
            this.env1();
        });
    }
}
