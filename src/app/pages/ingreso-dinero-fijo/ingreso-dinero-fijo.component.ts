import {Component, OnInit} from '@angular/core';
import {Gasto, Ingreso} from '../models/models.component';
import {AlertController, LoadingController, NavController, ToastController} from '@ionic/angular';
import {FirebaseService} from '../../services/firebase.service';
import firebase from 'firebase';
import {AuthenticationService} from '../../services/authentication.service';
import {IngresoDineroComponent} from '../ingreso-dinero/ingreso-dinero.component';
import {ActivatedRoute} from '@angular/router';
import {InteractionService} from '../../services/interaction.service';
import {NotificationsService} from '../../services/notifications.service';

@Component({
    selector: 'app-ingreso-dinero',
    templateUrl: './ingreso-dinero-fijo.component.html',
    styleUrls: ['./ingreso-dinero.component-fijo.scss'],
})
export class IngresoDineroFijoComponent implements OnInit {

    // typeFrecuencia: 'diario' | 'semanal' | 'mensual' = null;

    loading: any;
    tabID = null;
    orden = [];
    ingresosFijos: Ingreso[] = [];
    newIngresoFijo: Ingreso = {
        categoria: null,
        monto_ingreso: null,
        descripcion_ingreso: null,
        tipo_transaccion: 'ingreso-fijo',
        fecha: new Date(),
        id: this.firebaseService.getId()
    };

    constructor(private navCtrl: NavController,
                public firebaseService: FirebaseService,
                public loadingController: LoadingController,
                public toastController: ToastController,
                public alertController: AlertController,
                public auth: AuthenticationService,
                public route: ActivatedRoute,
                public interactionService: InteractionService,
                public notificationsService: NotificationsService
    ) {
    }

    ngOnInit() {
        this.auth.stateUser().subscribe(res => {
            if (res) {
                this.getIngresosFijos();
            }
        });
    }

    goBack() {
        this.navCtrl.back();
    }

    //Obtener primer dia del proximo mes
    async guardarIngresoFijo() {
        const uid = await this.auth.getUid();
        const ruta = 'Usuarios/' + uid + '/Ingresos Fijos';
        // this.newGasto.typeFrecuencia = this.typeFrecuencia;
        this.firebaseService.createDoc(this.newIngresoFijo, ruta, this.newIngresoFijo.id).then(res => {
            this.presentToast('Guardado con exito');
            this.loading.dismiss();
            this.notificationsService.notificacionInfo = true;
            this.nuevoIn();
            // const suceso = new Date().toLocaleString();
            // this.notificationsService.guardarNotification('Se ha guardado el ingreso fijo' +
            //     this.newIngresoFijo.descripcion_ingreso, 'informacion', true, suceso)
        })
        //     .catch(error => {
        //     this.presentToast('Guardado con exito');
        // });
    }

    async getIngresosFijos() {
        const uid = await this.auth.getUid();
        const ruta = 'Usuarios/' + uid + '/Ingresos Fijos';
        this.firebaseService.getCollection<Ingreso>(ruta).subscribe(res => {
            this.ingresosFijos = res;
        });
    }


    // selecfrecuencia(type: 'diario' | 'semanal' | 'mensual') {
    //     this.typeFrecuencia = type;
    // }

    nuevoIn() {
        // this.enableNewGasto = true;
        this.newIngresoFijo = {
            categoria: null,
            descripcion_ingreso: null,
            monto_ingreso: null,
            tipo_transaccion: 'ingreso-fijo',
            fecha: new Date(),
            id: this.firebaseService.getId()
        };
    }

    async deleteIngreso(ingreso: Ingreso) {
        const uid = await this.auth.getUid();
        const ruta = 'Usuarios/' + uid + '/Ingresos Fijos';
        const alert = await this.alertController.create({
            cssClass: 'normal',
            header: 'Eliminar ingreso fijo',
            message: 'Esta acción <strong>eliminará</strong> este ingreso fijo',
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (blah) => {
                    }
                }, {
                    text: 'Confirmar',
                    handler: () => {
                        this.firebaseService.deleteDoc(ruta, ingreso.id).then(res => {
                            this.presentToast('Eliminado con exito');
                            this.alertController.dismiss();
                        }).catch(error => {
                            // this.interactionService.presentToast('No se pudo eliminar');
                        });
                    }
                }
            ]
        });
        await alert.present();
    }
    async presentLoading() {
        this.loading = await this.loadingController.create({
            cssClass: 'normal',
            message: 'Guardando...'
        });
        await this.loading.present();
        //
        // const { role, data } = await loading.onDidDismiss();
        // console.log('Loading dismissed!');
    }

    async presentToast(msg: string) {
        const toast = await this.toastController.create({
            message: msg,
            cssClass: 'normal',
            duration: 1000,
        });
        toast.present();
    }

}
