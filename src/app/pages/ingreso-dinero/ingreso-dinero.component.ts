import {Component, Input, OnInit} from '@angular/core';
import {Gasto, Ingreso} from '../models/models.component';
import {AlertController, LoadingController, NavController, ToastController} from '@ionic/angular';
import {FirebaseService} from '../../services/firebase.service';
import firebase from 'firebase';
import {AuthenticationService} from '../../services/authentication.service';
import {IngresoDineroFijoComponent} from '../ingreso-dinero-fijo/ingreso-dinero-fijo.component';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'app-ingreso-dinero',
    templateUrl: './ingreso-dinero.component.html',
    styleUrls: ['./ingreso-dinero.component.scss'],
})
export class IngresoDineroComponent implements OnInit {


    ingresos: Ingreso[] = [];
    loading: any;
    orden:any[] = []
    tabID = null;



    constructor(private navCtrl: NavController,
                public firebaseService: FirebaseService,
                public loadingController: LoadingController,
                public toastController: ToastController,
                public alertController: AlertController,
                public auth: AuthenticationService,
                public route: ActivatedRoute,
                /*public inFijos: IngresoDineroFijoComponent*/) {
    }

    ngOnInit() {
        this.route.queryParams
            .subscribe(params => {
                console.log(Array.isArray(params))
                console.log('params', params); // { order: "popular"
                this.auth.stateUser().subscribe(res => {
                    if (res) {
                        const path = 'Usuarios/' + res.uid + '/Ingresos'
                        const fechaActual = new Date();
                        this.tabID = fechaActual.getMonth();
                        console.log(this.tabID);
                        console.log('this.tabId es:', this.tabID);
                        //Fecha inicio
                        const fechaInicio = new Date();
                        fechaInicio.setMonth(+params.mesElegido, 1);
                        fechaInicio.setHours(0, 0, 0);

                        //Fecha final
                        const fechaFin = new Date();
                        fechaFin.setMonth(+params.mesElegido, 30);
                        fechaFin.setHours(23, 59, 59);

                        this.firebaseService.getCollection2Query<Ingreso>(path, "fecha", '>=', fechaInicio,
                            'fecha', '<=', fechaFin).subscribe(res => {
                            this.ingresos = res;
                            this.ingresos.forEach(ingreso => {
                                ingreso.fecha = new Date(ingreso.fecha.seconds * 1000);
                            });
                            this.ordenar()
                        })
                    }
                });
            });
    }

    goBack() {
        this.navCtrl.back();
    }

    slideOpts = {
        slidesPerView: 3.2
    };


    newIngreso: Ingreso = {
        categoria: null,
        descripcion_ingreso: null,
        monto_ingreso: null,
        tipo_transaccion: 'ingreso',
        fecha: new Date(),
        id: this.firebaseService.getId(),
    };

    async guardarIngresos() {
        const uid = await this.auth.getUid();
        const ruta = 'Usuarios/' + uid + '/Ingresos';
        this.firebaseService.createDoc(this.newIngreso, ruta, this.newIngreso.id).then(res => {
            this.presentToast('Guardado con exito');
        })
        this.nuevoIn();
    }


    ordenar(){
        this.orden = [];
        this.ingresos.forEach(ingreso => {
            this.orden.push(ingreso);
        });
        this.orden.sort((a, b) => {
            if (a.fecha > b.fecha) {
                return -1;
            }
            if (a.fecha < b.fecha) {
                return 1;
            }
            if (a.fecha == b.fecha) {
                return 0;
            }
        });
    }

    nuevoIn() {
        // this.enableNewGasto = true;
        this.newIngreso = {
            categoria: null,
            descripcion_ingreso: null,
            monto_ingreso: null,
            tipo_transaccion: 'ingreso',
            fecha: new Date(),
            id: this.firebaseService.getId()
        };
    }


    async deleteIngreso(ingreso: Ingreso) {
        const uid = await this.auth.getUid();
        const ruta = 'Usuarios/' + uid + '/Ingresos';
        const alert = await this.alertController.create({
            cssClass: 'normal',
            header: 'Eliminar ingreso',
            message: 'Esta acción <strong>eliminará</strong> este ingreso',
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (blah) => {
                    }
                }, {
                    text: 'Confirmar',
                    handler: () => {
                        this.firebaseService.deleteDoc(ruta, ingreso.id).then(res => {
                            this.presentToast('Eliminado con exito');
                            this.alertController.dismiss();
                        }).catch(error => {
                            this.presentToast('No se pudo eliminar');
                        });
                    }
                }
            ]
        });

        await alert.present();
    }

    async presentToast(msg: string) {
        const toast = await this.toastController.create({
            message: msg,
            cssClass: 'normal',
            duration: 2000
        });
        toast.present();
    }
}
