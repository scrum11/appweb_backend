import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {Usuario} from '../../interfaces';
import {FirebaseService} from '../../services/firebase.service';
import {AuthenticationService} from '../../services/authentication.service';
import {onErrorResumeNext} from 'rxjs';
import {error} from 'protractor';
import {catchError} from 'rxjs/operators';
import {AlertController} from '@ionic/angular';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})

export class LoginPage implements OnInit {
        correo: string;
        password: string;
        incorrectCreed = false;

  constructor(private router: Router, public firebaseService: FirebaseService,
              private auth: AuthenticationService,
              public alertController: AlertController
  ) { }

  ngOnInit() {

  }

  async alertControl (header, message){
      const alert = await this.alertController.create({
          header,
          message,
          buttons: ['OK'],
      });
      await alert.present();
  }

    async login() {
        const res = await this.auth.login(this.correo, this.password).catch( error => {
            this.alertControl('Credenciales fallidas', 'Intentalo nuevamente')
        });

        if (res) {
            this.router.navigateByUrl('/success');
        }
    }

  goToHome() {
      this.router.navigate(['/tabs']);
  }

  goToRegister() {
      this.router.navigate(['/register']);
  }

  goToForgot() {
      this.router.navigate(['/forgot']);
  }

}
