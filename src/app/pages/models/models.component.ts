import {promise} from 'protractor';

export interface Usuario {
    usuario: null,
    edad: null,
    correo: null,
    password: null,
    uid: null,
    perfil: 'visitante'
}

export interface Gasto {
    // gasto_inmmediato: boolean;
    // gasto_semanal: boolean;
    // gasto_mensual: boolean;
    typeFrecuencia ?: string;
    categoria: 'Hogar' | 'Suscripción'| 'Compras' |  'Educación' | 'Comida' | 'Mascotas' | 'Entretenimiento' | 'Transporte' | 'Deudas' | 'Otros';
    descripcion_gasto: string;
    monto_gasto: number;
    tipo_transaccion: string;
    fecha: any;
    id: string;
}

export interface Ingreso {
    typeFrecuencia ?: string;
    categoria: boolean;
    descripcion_ingreso: string;
    tipo_transaccion: string;
    fecha: any;
    monto_ingreso: number;
    id: string;
}

export interface Categorias {
    hogar: boolean;
    comida: boolean;
    educacion: boolean;
    compras: boolean;
    transporte: boolean;
    entretenimiento: boolean;
    mascotas: boolean;
    suscripcion: boolean;
    deudas: boolean;
    otros: boolean;
    id: string;
}

export interface Notificacion {
    descripcion: string,
    tipo: string; //'alerta' | 'logro' |  'presupuesto' | 'informacion' | 'recordatorio';
    estado: boolean,
    fecha: any,
    id: any
}
