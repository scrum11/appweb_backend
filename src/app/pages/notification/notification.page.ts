import {Component, OnInit} from '@angular/core';
import {AlertController, LoadingController, NavController, ToastController} from '@ionic/angular';
import {NotificationsService} from '../../services/notifications.service';
import {Categorias, Gasto, Notificacion} from '../models/models.component';
import {InteractionService} from '../../services/interaction.service';
import {FirebaseService} from '../../services/firebase.service';
import {AuthenticationService} from '../../services/authentication.service';
import {ActivatedRoute, Router} from '@angular/router';
import {async} from 'rxjs/internal/scheduler/async';

@Component({
    selector: 'app-notification',
    templateUrl: './notification.page.html',
    styleUrls: ['./notification.page.scss'],
})
export class NotificationPage implements OnInit {

    notificaciones = [];
    mensajes = [];
    notificacionPresupuesto = null;
    notificacionInfo = null;
    notificacionLogro = null

    newNotification: Notificacion = {
        descripcion: null,
        tipo: null,
        estado: null,
        fecha: null,
        id: this.auth.getUid()
    };

    constructor(private navCtrl: NavController,
                public firebaseService: FirebaseService,
                public loadingController: LoadingController,
                public toastController: ToastController,
                public alertController: AlertController,
                private auth: AuthenticationService,
                public route: ActivatedRoute,
                public router: Router,
                public interactionService:InteractionService,
                public notificationsService:NotificationsService) {
    }

    ngOnInit() {
        this.getNotificacion();
    }

    async getNotificacion() {
        this.notificationsService.notificacionPresupuesto = this.notificacionPresupuesto;
        console.log('this.notificacionPresupuesto', this.notificacionPresupuesto);
        this.notificationsService.notificacionLogro = this.notificacionLogro;
        this.notificationsService.notificacionInfo = this.notificacionInfo;
    //     const uid = await this.auth.getUid();
    //     const ruta = 'Usuarios/' + uid + '/Notificaciones';
    //     this.mensajes = []
    //     this.firebaseService.getCollection(ruta).subscribe(res => {
    //         this.mensajes = res;
    //     });
    //     console.log('mensajes', this.mensajes);
    }

    async deleteNotificacion(notificacion: Notificacion) {
        const uid = await this.auth.getUid();
        const ruta = 'Usuarios/' + uid + '/Notificaciones';
        const alert = await this.alertController.create({
            cssClass: 'normal',
            header: 'Notificación',
            message: 'Esta acción <strong>eliminará</strong> esta notificación',
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (blah) => {
                    }
                }, {
                    text: 'Confirmar',
                    handler: () => {
                        this.firebaseService.deleteDoc(ruta, notificacion.id).then(res => {
                            this.interactionService.presentToast('Eliminado con exito');
                            this.alertController.dismiss();
                        }).catch(error => {
                            this.interactionService.presentToast('No se pudo eliminar');
                        });
                    }
                }
            ]
        });
        await alert.present();
    }
}
