import {Router} from '@angular/router';
import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../../services/authentication.service';
import firebase from 'firebase';
import {FirebaseService} from '../../services/firebase.service';
import {AlertController} from '@ionic/angular';
import {InteractionMode} from 'chart.js';
import {Usuario} from '../models/models.component';
import {InteractionService} from '../../services/interaction.service';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.page.html',
    styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

    constructor(private auth: AuthenticationService,
                private firestoreService: FirebaseService,
                public alertController: AlertController,
                public interactionService: InteractionService,
                public router: Router
    ) {
    }

    uid: string = null;
    info: Usuario = null;


    async ngOnInit() {
        this.auth.stateUser().subscribe(res => {
            this.getUid();
        });
    }

    async getUid() {
        const uid = await this.auth.getUid();
        if (uid) {
            this.uid = uid;
            this.getInfoUser();
        } else {
            console.log('No existe uid');
        }
    }

    getInfoUser() {
        const path = 'Usuarios/';
        const id = this.uid;
        this.firestoreService.getDoc<Usuario>(path, id).subscribe(res => {
            if (res) {
                this.info = res;
            }
            console.log('datos son:', res.usuario);
        });
    }

    // async editarInfo() {
    //     const alert = await this.alertController.create({
    //         header: 'Editar nombre',
    //         inputs: [
    //             {
    //                 name: 'nombre',
    //                 placeholder: 'Introduzca su nombre para cambiarlo',
    //                 type: 'text'
    //             },
    //             {
    //                 type: 'number',
    //                 name: 'edad',
    //                 placeholder: 'Introduzca su edad para cambiarla',
    //                 min: 1,
    //             },
    //
    //         ],
    //         buttons: [
    //             {
    //                 text: 'Cancelar',
    //                 role: 'cancel',
    //                 handler: () => {
    //                     this.handlerMessage = 'Alert canceled';
    //                 },
    //             },
    //             {
    //                 text: 'Aceptar',
    //                 role: 'confirm',
    //                 handler: (ev) => {
    //                     console.log('ev', ev);
    //                     this.handlerMessage = 'Alert confirmed';
    //                     this.guardarCambios(ev.nombre);
    //                 },
    //             },
    //             {
    //                 text: 'Aceptar',
    //                 role: 'confirm',
    //                 handler: (ev) => {
    //                     console.log('ev', ev);
    //                     this.handlerMessage = 'Alert confirmed';
    //                     this.guardarCambios(ev.edad);
    //                 },
    //             },
    //         ],
    //     });
    //     await alert.present();
    // }


    async guardarCambios(edad ?: string, nombre ?: any) {
        const path = 'Usuarios/' + this.uid;
        const id = this.uid;
        const updateDoc = {
            edad: edad,
            nombre: nombre
        };
        // updateDoc[edad] = input;
        await this.firestoreService.updateDoc(path, id, updateDoc).then(() => {
            this.interactionService.presentToast("Actualizado con exito");
            this.interactionService.closeLoading();
        });
    }
}
