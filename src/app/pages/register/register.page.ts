import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../../services/authentication.service';
import { Usuario } from 'src/app/interfaces';
import {FirebaseService} from '../../services/firebase.service';
import {AlertController} from '@ionic/angular';
import {logging} from 'protractor';
import {loggedIn} from '@angular/fire/auth-guard';
import firebase from 'firebase';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  datos = {
    usuario: null,
    edad: null,
    correo: null,
    password: null,
    uid: null,
    perfil: 'visitante'
  }

  constructor( private auth: AuthenticationService,
               private route:Router,
               private firestore:  FirebaseService,
               public alertController: AlertController) { }

  ngOnInit() {
  }

  async alertControl (header, message){
    const alert = await this.alertController.create({
      header,
      message,
      buttons: ['OK'],
    });
    await alert.present();
  }

  async registrar(){
    const resp = await this.auth.registrarUser(this.datos).catch(error => {
          this.alertControl('Registro fallido', 'Inténtalo nuevamente');
        })

    if (resp) {
      await this.alertControl('Éxito al crear usuario', 'Porfavor inicia sesión para continuar')
      const path = 'Usuarios';
      const id= resp.user.uid;
      this.datos.uid = id
      //this.datos.password = null;
      await this.firestore.createDoc(this.datos, path, id).catch(
      )
      this.route.navigate(['/login']);
    }
  }

  irLogin(){
    this.route.navigate(['/login']);
  }
}
