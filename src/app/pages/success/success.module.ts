import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SuccessPageRoutingModule } from './success-routing.module';

import { SuccessPage } from './success.page';
import {ConfiguracionGastosPageModule} from '../configracion-gastos/configuracion-gastos.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        SuccessPageRoutingModule,
        ConfiguracionGastosPageModule
    ],
  declarations: [SuccessPage]
})
export class SuccessPageModule {}
