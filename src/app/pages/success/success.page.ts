import { Router } from '@angular/router';
import {Component, Input, OnInit} from '@angular/core';
import {Content} from '@angular/compiler/src/render3/r3_ast';

@Component({
  selector: 'app-success',
  templateUrl: './success.page.html',
  styleUrls: ['./success.page.scss'],
})
export class SuccessPage implements OnInit {

  @Input("content") protected content: Content;



  constructor(private router: Router) { }

  ngOnInit() {}


  // goToHome() {
  //     this.router.navigate(['/tabs/home']);
  // }

}
