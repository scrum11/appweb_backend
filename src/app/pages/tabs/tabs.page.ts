import { Component } from '@angular/core';
import { Router } from '@angular/router';
import {InteractionService} from '../../services/interaction.service';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {
  tabID;
  constructor(
    private router: Router,
    public interactionService: InteractionService
  ) {
  }
  goToHome(){
    this.router.navigate(['/tabs/home'])
    console.log(this.interactionService.mesElegido)
  }
  goToGrafica(){
    this.router.navigate(['/tabs/grafica'])
    console.log(this.interactionService.mesElegido)
  }
  goToLogros(){
    this.router.navigate(['/tabs/wallet'])
    console.log(this.interactionService.mesElegido)
  }
  goToNotific(){
    this.router.navigate(['/tabs/notification'])
    console.log(this.interactionService.mesElegido)
  }
  goToProfile(){
    this.router.navigate(['/tabs/profile'])
  }


  tabChange(eve) {
    console.log(eve.tab);
    this.tabID = eve.tab;
  }

  home() {
    this.router.navigate(['tabs']);
  }
}
