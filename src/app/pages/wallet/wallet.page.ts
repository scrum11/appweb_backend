import {Router} from '@angular/router';
import {Component, OnInit} from '@angular/core';
import {NavController} from '@ionic/angular';
import {FirebaseService} from '../../services/firebase.service';
import {AuthenticationService} from '../../services/authentication.service';
import {InteractionService} from '../../services/interaction.service';
import {Gasto, Ingreso} from '../models/models.component';
import {NotificationsService} from '../../services/notifications.service';

@Component({
    selector: 'app-wallet',
    templateUrl: './wallet.page.html',
    styleUrls: ['./wallet.page.scss'],
})
export class WalletPage implements OnInit {


    slideOpts = {
        slidesPerView: 1.6
    };

    gastos: Gasto[] = [];
    ingresos: Ingreso[] = [];
    sumaG: number = null;
    sumaI: number = null;
    tabID = null;
    // Suma de elementos de una categoria
    catSuscripcion = null;
    catHogar = null;
    catEducacion = null;
    catComida = null;
    catMascotas = null;
    catEntretenimiento = null;
    catTransporte = null;
    catDeudas = null;
    catOtros = null;
    exControl = false;
    //Calculo de porcantaje recomendado para cada categoria
    sumaCatHogar = null;
    sumaCatSuscripcion = null;
    sumaCatEntretenimiento = null;
    sumaCatDeudas = null;
    sumaCatComida = null;
    sumaCatMascotas = null;
    sumaCatTransporte = null;
    sumaCatOtros = null;
    sumaCatEducacion = null;
    kip = null;
    mesActual = null;
    //Final mes presente

    //Mes antiguo
    _gastos: Gasto[] = [];
    _ingresos: Ingreso[] = [];
    _sumaG: number = null;
    _sumaI: number = null;
    // Suma de elementos de una categoria
    _catSuscripcion = null;
    _catHogar = null;
    _catEducacion = null;
    _catComida = null;
    _catMascotas = null;
    _catEntretenimiento = null;
    _catTransporte = null;
    _catDeudas = null;
    _catOtros = null;
    _exControl = false;
    //Calculo de porcantaje recomendado para cada categoria
    _sumaCatHogar = null;
    _sumaCatSuscripcion = null;
    _sumaCatEntretenimiento = null;
    _sumaCatDeudas = null;
    _sumaCatComida = null;
    _sumaCatMascotas = null;
    _sumaCatTransporte = null;
    _sumaCatOtros = null;
    _sumaCatEducacion = null;

    restaEntreMesesGasto = null;
    avanzando = false;
    buenosHabitos = false;
    restaEntreMesesIngreso = null;
    conscienteSoy = false;
    ahorrador = false;
    bolsilloFeliz = false;
    labelEmpty = false;


    constructor(private router: Router,
                private navCtrl: NavController,
                public firebaseService: FirebaseService,
                public auth: AuthenticationService,
                public interactionService: InteractionService,
                public notificationsService:NotificationsService
    ) {
    }

    ionViewWillEnter() {
        this.primerDiaMesAnterior(this.interactionService.mesElegido);
        if (this.kip != null){
        this.operarMes(this.interactionService.mesElegido, this.kip); //kip es el mes anterior al actual
        }
        console.log('Mes elegido', this.interactionService.mesElegido);
        console.log('Mes anterior', this.kip);
    }

    ngOnInit() {
        // this.primerDiaMesAnterior();
        // if (this.kip != null) {
        //     this.operarMes(this.interactionService.mesElegido, this.kip);
        // }
        // console.log("Mes elegido", this.interactionService.mesElegido);
        // console.log("Mes anterior", this.kip);
    }

    operarMes(mes: number, antes: number) {
        console.log('Mes de ahora = ', mes);
        console.log('Mes de antes = ', antes);
        this.auth.stateUser().subscribe(res => {
            if (res) {
                const pathIngresos = 'Usuarios/' + res.uid + '/Ingresos';
                const pathGastos = 'Usuarios/' + res.uid + '/Gastos';

                //if (this.tabID) {
                //Fecha inicio
                const fechaInicio = new Date();
                fechaInicio.setMonth(mes, 1);
                fechaInicio.setHours(0, 0, 0);
                const formatter = new Intl.DateTimeFormat('ec', {month: 'long'});
                this.mesActual = formatter.format(fechaInicio);
                //Fecha final
                const fechaFin = new Date();
                fechaFin.setMonth(mes, 30);
                fechaFin.setHours(23, 59, 59);

                //Fecha inicio anterior mes
                const _fechaInicio = new Date();
                _fechaInicio.setMonth(antes, 1);
                _fechaInicio.setHours(0, 0, 0);

                //Fecha final anterior mes
                const _fechaFin = new Date();
                _fechaFin.setMonth(antes, 30);
                _fechaFin.setHours(23, 59, 59);


                //Operando el mes actual
                this.firebaseService.getCollection2Query<Ingreso>(pathIngresos, 'fecha', '>=', fechaInicio,
                    'fecha', '<=', fechaFin).subscribe(res => {
                    this.ingresos = res;
                    this.sumaI = 0;
                    this.ingresos.forEach(ingreso => {
                        this.sumaI += ingreso.monto_ingreso;
                    });
                    console.log('Suma de ingresos', this.sumaI);
                });

                //Operando el mes anterior
                this.firebaseService.getCollection2Query<Ingreso>(pathIngresos, 'fecha', '>=', _fechaInicio,
                    'fecha', '<=', _fechaFin).subscribe(res => {
                    this._ingresos = res;
                    this._sumaI = 0;
                    this._ingresos.forEach(ingreso => {
                        this._sumaI += ingreso.monto_ingreso;
                    });
                    console.log('Suma de ingresos mes anterior', this._sumaI);
                });

                this.restaEntreMesesIngreso = this._sumaI - this.sumaI;
                console.log('this.restaEntreMesesIngreso', this.restaEntreMesesIngreso);
                if (this.restaEntreMesesIngreso >= 10 && this.restaEntreMesesIngreso < 20) {
                    this.conscienteSoy = true;
                    this.notificationsService.notificacionLogro = true;
                    // const suceso = new Date().toLocaleString();
                    // this.notificationsService.guardarNotification('Ha obtenido el logro "Consciente Soy"',
                    //     'logro', true, suceso)
                }
                if (this.restaEntreMesesIngreso >= 20) {
                    this.ahorrador = true;
                }

                this.firebaseService.getCollection2Query<Gasto>(pathGastos, 'fecha', '>=', fechaInicio,
                    'fecha', '<=', fechaFin).subscribe(res => {
                    this.gastos = res;
                    this.sumaG = 0;
                    this.catEducacion = 0;
                    this.catOtros = 0;
                    this.catEntretenimiento = 0;
                    this.catSuscripcion = 0;
                    this.catDeudas = 0;
                    this.catComida = 0;
                    this.catMascotas = 0;
                    this.catTransporte = 0;
                    this.catHogar = 0;
                    this.gastos.forEach(gasto => {
                        this.sumaG += gasto.monto_gasto;
                        if (gasto.categoria == 'Educación') {
                            this.catEducacion += gasto.monto_gasto;
                            this.exControl = true;
                            console.log('this.catEducacion', this.catEducacion);
                            this.env9();
                        }
                        if (gasto.categoria == 'Hogar') {
                            this.catHogar += gasto.monto_gasto;
                            this.exControl = true;
                            this.env1();
                        }
                        if (gasto.categoria == 'Suscripción') {
                            this.catSuscripcion += gasto.monto_gasto;
                            this.exControl = true;
                            this.env2();
                        }
                        if (gasto.categoria == 'Comida') {
                            this.catComida += gasto.monto_gasto;
                            this.exControl = true;
                            this.env5();
                        }
                        if (gasto.categoria == 'Mascotas') {
                            this.catMascotas += gasto.monto_gasto;
                            this.exControl = true;
                            console.log('catMascotas', this.catMascotas);
                            this.env6();
                        }
                        if (gasto.categoria == 'Entretenimiento') {
                            this.catEntretenimiento += gasto.monto_gasto;
                            this.exControl = true;
                            this.env3();
                        }
                        if (gasto.categoria == 'Transporte') {
                            this.catTransporte += gasto.monto_gasto;
                            this.exControl = true;
                            this.env7();
                        }
                        if (gasto.categoria == 'Deudas') {
                            this.catDeudas += gasto.monto_gasto;
                            this.exControl = true;
                            console.log('catdeudas', this.catDeudas);
                            this.env4();
                        }
                        if (gasto.categoria == 'Otros') {
                            this.catOtros += gasto.monto_gasto;
                            this.exControl = true;
                            this.env8();
                        }
                    });
                    console.log('Suma de ingresos mes actual', this.sumaG);
                    this.sumaCatEducacion = 0;
                    this.sumaCatEducacion = (this.sumaI * 0.08).toFixed(2);
                    this.sumaCatOtros = 0;
                    this.sumaCatOtros = (this.sumaI * 0.05).toFixed(2);
                    this.sumaCatTransporte = 0;
                    this.sumaCatTransporte = (this.sumaI * 0.10).toFixed(2);
                    this.sumaCatMascotas = 0;
                    this.sumaCatMascotas = (this.sumaI * 0.05).toFixed(2);
                    this.sumaCatComida = 0;
                    this.sumaCatComida = (this.sumaI * 0.30).toFixed(2);
                    this.sumaCatDeudas = 0;
                    this.sumaCatDeudas = (this.sumaI * 0.05).toFixed(2);
                    this.sumaCatEntretenimiento = 0;
                    this.sumaCatEntretenimiento = (this.sumaI * 0.07).toFixed(2);
                    this.sumaCatSuscripcion = 0;
                    this.sumaCatSuscripcion = (this.sumaI * 0.04).toFixed(2);
                    this.sumaCatHogar = 0;

                    console.log(this.catOtros, '<', this.sumaCatOtros);
                    if (this.catEducacion < this.sumaCatEducacion && this.catOtros < this.sumaCatOtros && this.catTransporte < this.sumaCatTransporte &&
                        this.catMascotas < this.sumaCatMascotas && this.catComida < this.sumaCatComida && this.catDeudas < this.sumaCatDeudas &&
                        this.catEntretenimiento < this.sumaCatEntretenimiento && this.catSuscripcion < this._sumaCatSuscripcion && this.catHogar < this.sumaCatHogar) {
                        this.bolsilloFeliz = true;
                    }
                    if (this.bolsilloFeliz == false && this.avanzando == false && this.ahorrador == false && this.conscienteSoy == false) {
                        this.labelEmpty = true;
                    }
                });
                //}

                // this.sumaCatHogar = (this.sumaI * 0.15).toFixed(2);


                this.firebaseService.getCollection2Query<Gasto>(pathGastos, 'fecha', '>=', _fechaInicio,
                    'fecha', '<=', _fechaFin).subscribe(res => {
                    this._gastos.forEach(gasto => {
                        this._sumaG += gasto.monto_gasto;
                    });
                    this.restaEntreMesesGasto = this._sumaG - this.sumaG;
                    console.log('this.sumaG ->', this.sumaG);

                    console.log('this._sumaG - this.sumaG', this.restaEntreMesesGasto);
                    if (this.restaEntreMesesGasto >= 10 && this.restaEntreMesesGasto < 20) {
                        this.avanzando = true;
                        this.notificationsService.notificacionLogro = true;
                        // const suceso = new Date().toLocaleString();
                        // this.notificationsService.guardarNotification('Ha obtenido el logro "Avanzando"', 'logro',
                        //     true, suceso)
                    }
                    //Logro 1
                    if (this.restaEntreMesesGasto >= 20) {
                        this.buenosHabitos = true;
                        //Enviar una notificacion alertando del suceso
                        //Logro 2
                        //fn gasts
                    }
                });
            }
        });
    }

    primerDiaMesAnterior(mes: number) {
        const fechaInicio = new Date();
        fechaInicio.setMonth(mes, 1);
        this.kip = fechaInicio.getMonth() - 1;
        if (this.kip == 0) {
            this.kip = null;
        }
        console.log('->', this.kip);
    } //        this.kip =  new Date(date.getFullYear(), date.getMonth() - 1, 1);


    // restaEntreMesesIngresoA() {
    //     this.restaEntreMesesIngreso = this._sumaI - this.sumaI;
    // }

    env1() {
        this.sumaCatHogar = 0;
        this.sumaCatHogar = (this.sumaI * 0.15).toFixed(2);
    }

    env2() {
        this.sumaCatSuscripcion = 0;
        this.sumaCatSuscripcion = (this.sumaI * 0.04).toFixed(2);
    }

    env3() {
        this.sumaCatEntretenimiento = 0;
        this.sumaCatEntretenimiento = (this.sumaI * 0.07).toFixed(2);
    }


    env4() {
        this.sumaCatDeudas = 0;
        this.sumaCatDeudas = (this.sumaI * 0.05).toFixed(2);
    }


    env5() {
        this.sumaCatComida = 0;
        this.sumaCatComida = (this.sumaI * 0.30).toFixed(2);
    }


    env6() {
        this.sumaCatMascotas = 0;
        this.sumaCatMascotas = (this.sumaI * 0.05).toFixed(2);
    }


    env7() {
        this.sumaCatTransporte = 0;
        this.sumaCatTransporte = (this.sumaI * 0.10).toFixed(2);
    }


    env8() {
        this.sumaCatOtros = 0;
        this.sumaCatOtros = (this.sumaI * 0.05).toFixed(2);
    }


    env9() {
        this.sumaCatEducacion = 0;
        this.sumaCatEducacion = (this.sumaI * 0.08).toFixed(2);
        console.log('this.sumaCatEducacion', this.sumaCatEducacion);
    }
}
