import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { WelcomePageRoutingModule } from './welcome-routing.module';

import { WelcomePage } from './welcome.page';
import {ConfiguracionGastosPageModule} from '../configracion-gastos/configuracion-gastos.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        WelcomePageRoutingModule,
        ConfiguracionGastosPageModule,
    ],
  declarations: [WelcomePage]
})
export class WelcomePageModule {}
