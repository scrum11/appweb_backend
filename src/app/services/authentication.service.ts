import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import firebase from 'firebase';
@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  constructor(public auth: AngularFireAuth) {}

  login(correo: string, password: string) {
    return  this.auth.signInWithEmailAndPassword(correo,password);
  }

  singIn(correo: string, password: string) {
    return this.auth.createUserWithEmailAndPassword(correo, password);
  }

  stateUser(){
    return this.auth.authState;
  }

  async logout(){
    await this.auth.signOut();
    window.location.reload();
    //const user = await this.auth.currentUser;
    //user.updatePassword;
  }

  registrarUser(datos){
   return this.auth.createUserWithEmailAndPassword(datos.correo, datos.password);
  }

  async getUid(){
    const user = await this.auth.currentUser;
    if (user){
      return user.uid;
    } else {
      return null;
    }
  }
}
