import {Injectable} from '@angular/core';
import {AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection} from '@angular/fire/firestore';
import firebase from 'firebase';
import firestore = firebase.firestore;
import database = firebase.database;

@Injectable({
    providedIn: 'root'
})

export class FirebaseService {

    constructor(public database: AngularFirestore) {
    }

    createDoc<tipo>(data: any, path: string, id: string) {
        const userCollection: AngularFirestoreCollection = this.database.collection(path);
        return userCollection.doc(id).set(data);
    }

    createIdDoc(): string {
        return this.database.createId();
    }

    getDoc<tipo>(path: string, id: string) {
        const collection = this.database.collection(path);
        return collection.doc<tipo>(id).valueChanges();
    }

    deleteDoc(path: string, id: string) {
        const collection = this.database.collection(path);
        return collection.doc(id).delete();
    }

    updateDoc(path: string, id: string, data: any) {
        const collection = this.database.collection(path);
        return collection.doc(id).update(data);
    }

    getId() {
        return this.database.createId();
    }

    getCollection<tipo>(path: string) {
        const collection = this.database.collection<tipo>(path);
        return collection.valueChanges()
    }

    getCollection2Query<tipo>(path: string,
                              parametro1: string, condicion1: any, busqueda1: any,
                              parametro2: string, condicion2: any, busqueda2: any,
    ) {
        const itemsCollection: AngularFirestoreCollection<tipo> =
            this.database.collection<tipo>(path
                , ref => ref.where(parametro1, condicion1, busqueda1)
                    .where(parametro2, condicion2, busqueda2)  //
            );
        return itemsCollection.valueChanges();
    }

    getCollectionParam<tipo>(path: string, order: string) {
        const dataCollection: AngularFirestoreCollection =
            this.database.collection(path,
                ref => ref
                    .orderBy(order, 'desc')
            );
        return dataCollection.valueChanges();
    }


//     // Create Data
//     const timestamp = firestore.FieldValue.serverTimestamp;
//
//     database.collection('date').add({ ...myData, createdAt: timestamp() })
//
// // Query
// database.collection('things').orderBy('createdAt').startAfter(today);

}
