import { Injectable } from '@angular/core';
import { LoadingController, ToastController } from '@ionic/angular';
import {LocalNotifications} from '@capacitor/local-notifications';

@Injectable({
  providedIn: 'root'
})
export class InteractionService {

  loading: any;
  mesElegido: number;
  //Para configuracion de gastos
  // hogarIsActive: boolean = null


  constructor(public toastController: ToastController,
              public loadingController: LoadingController,
              ) { }

  async presentToast(mensaje: string) {
    const toast = await this.toastController.create({
      message: mensaje,
      duration: 2500
    });
    await toast.present();
  }

  async presentLoading(mensaje: string) {
    this.loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: mensaje,

    });
    await this.loading.present();
  }

  async closeLoading() {
    await this.loading.dismiss(1000);
  }

  async notificacion(titulo: string, texto: string) {
    await LocalNotifications.schedule({
      notifications: [
        {
          title: titulo,
          body: texto,
          id: 1,
          extra: {
            data: ''
          },
          iconColor: '#Ff30a2',
          actionTypeId: 'RES_MSG'
        }
      ]
    });

  }


}
