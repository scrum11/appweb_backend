import {Injectable} from '@angular/core';

import {Subject} from 'rxjs';
import {LocalNotifications} from '@capacitor/local-notifications';
import {Notificacion} from '../pages/models/models.component';
import {AlertController, LoadingController, NavController, ToastController} from '@ionic/angular';
import {InteractionService} from './interaction.service';
import {FirebaseService} from './firebase.service';
import {AuthenticationService} from './authentication.service';
import {ActivatedRoute, Router} from '@angular/router';

// @ts-ignore
@Injectable({
    providedIn: 'root'
})
export class NotificationsService {

    descripcion: string = null;
    tipo: string = null;
    estado: boolean = null;
    fecha: string = null;

    notificacionPresupuesto:boolean = false
    notificacionLogro:boolean = false
    notificacionInfo:boolean = false

    constructor(private navCtrl: NavController,
                public interactionService: InteractionService,
                public firebaseService: FirebaseService,
                public loadingController: LoadingController,
                public toastController: ToastController,
                public alertController: AlertController,
                private auth: AuthenticationService,
                public route: ActivatedRoute,
                public router: Router,
                ) {
    }

    private alertSource = new Subject();
    alert$ = this.alertSource.asObservable();

    async guardarNotification(descripcion: string, tipo: string, estado: boolean, fecha: string) {
        const newNotification: Notificacion = {
            descripcion: descripcion,
            tipo: tipo,
            estado: estado,
            fecha: fecha,
            id: await this.firebaseService.getId()
        };
        const uid = await this.auth.getUid();
        const ruta = 'Usuarios/' + uid + '/Notificaciones';
        await this.interactionService.presentToast('Obteniendo notificaciones');
        this.firebaseService.createDoc<Notificacion>(newNotification, ruta, newNotification.id).then(res => {
            this.interactionService.closeLoading();
            // this.interactionService.presentToast('');
        }).catch(error => {
            // this.interactionService.presentToast('No se pudo guardar');
        });
    }

//
//
// async notificacion(message: string, texto: string, time: number = 1000) {  //Esto se le puede llamar desde otra funcion
//     this.alertSource.next(
//         await LocalNotifications.schedule({
//             notifications: [
//                 {
//                     title: message,
//                     body: texto,
//                     id: 1,
//                     extra: {
//                         data: ''
//                     },
//                     iconColor: '#Ff30a2',
//                     actionTypeId: 'RES_MSG'
//                 }
//             ]
//         })
//     );
// }
}

// async notificacion(titulo: string, texto: string) {
//     await LocalNotifications.schedule({
//         notifications: [
//             {
//                 title: titulo,
//                 body: texto,
//                 id: 1,
//                 extra: {
//                     data: ''
//                 },
//                 iconColor: '#Ff30a2',
//                 actionTypeId: 'RES_MSG'
//             }
//         ]
//     });
//
// }

