// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
export const environment = {
  production: true,
  firebaseConfig : {
    apiKey: 'AIzaSyBRUUJhZPFrl_1qz51669zoouqLOHHQFaU',
    authDomain: 'appfinanzas-4b049.firebaseapp.com',
    projectId: 'appfinanzas-4b049',
    storageBucket: 'appfinanzas-4b049.appspot.com',
    messagingSenderId: '35071491795',
    appId: '1:35071491795:web:2c6bf29195d5f3b0a45e26'
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
